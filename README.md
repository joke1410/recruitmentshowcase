# Recruitment showcase #

The project has been created fo recruitment purposes.

### Basic info ###

The application is a To Do list written with an "offline first" approach. However, the tasks are also synced via backend. 

The project consists of 2 parts:

* iOS application
* watchOS appplication

##### Disclaimer

*Backend part ([RecruitmentShowcaseBackend](https://bitbucket.org/joke1410/recruitmentshowcasebackend)) was written in a dummy way, the data is stored in memory so it disappears then the app is killed on the server. It is also shared across all of the clients of the API. I know there is much space for improvements but it is out of scope of this recruitment showcase project.*



### External dependencies managed via SPM ###

- [Swinject](https://github.com/Swinject/Swinject)
- [Quick](https://github.com/Quick/Quick) + [Nimble](https://github.com/Quick/Nimble)
- [Realm](https://github.com/realm)
- [combine-schedulers](https://github.com/pointfreeco/combine-schedulers)

