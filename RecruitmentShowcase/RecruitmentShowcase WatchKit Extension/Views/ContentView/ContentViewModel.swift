//
//  ContentViewModel.swift
//

import Foundation
import Combine
import WatchCommunicationProtocol

final class ContentViewModel: ObservableObject {
    
    @Published var isWatchSessionReachable: Bool = false
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    @Injected<WatchStateServiceInterface> private var watchState
    
    init(todos: [WatchTodo] = []) {
        subscribeToAppStateChanges()
    }
}

private extension ContentViewModel {
    
    func subscribeToAppStateChanges()  {
        watchState
            .isWatchSessionReachable
            .receive(on: DispatchQueue.main)
            .sink { [weak self] isReachable in
                self?.isWatchSessionReachable = isReachable
            }.store(in: &subscriptionBag)
    }
}
