//
//  ContentView.swift
//

import SwiftUI

struct ContentView: View {
    
    @InjectedObservedObject<ContentViewModel> private var viewModel
    
    @State private var selectedTodoId: String? = nil
    
    var body: some View {
        if viewModel.isWatchSessionReachable {
            TodoListView()
        } else {
            Text("Paired iPhone is out of range")
                .padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        ContentView()
    }
}
