//
//  TodoListView.swift
//

import SwiftUI
import WatchCommunicationProtocol

struct TodoListView: View {
    
    @InjectedObservedObject<TodoListViewModel> private var viewModel
    
    @State private var selectedTodoId: String? = nil
    
    var body: some View {
        Group {
            if viewModel.todos.isEmpty {
                Text("You have nothing to do!")
                    .padding()
            } else {
                list
            }
        }
        .navigationTitle("To do list")
    }
    
    private var list: some View {
        List {
            ForEach(viewModel.todos, id: \.self) { todo in
                ZStack {
                    NavigationLink(
                        tag: todo.id,
                        selection: $selectedTodoId,
                        destination: {
                            TodoDetailsView(todoId: todo.id)
                        },
                        label: { EmptyView() }
                    )
                    todoItem(for: todo)
                }
            }
        }
    }
    
    private func todoItem(for todo: WatchTodo) -> some View {
        Text(todo.title)
            .strikethrough(todo.isDone)
            .foregroundColor(todo.isDone ? .gray : .white)
    }
}

struct TodoListView_Previews: PreviewProvider {
    
    static var previews: some View {
        Preview()
    }
    
    struct Preview: View {
        
        init() {
            DependencyContainer.container?.register(TodoListViewModel.self) { _ in
                TodoListViewModel(todos: [WatchTodo(id: "id", title: "Do something", details: nil, isDone: true)])
            }.inObjectScope(.container)
        }
        
        var body: some View {
            TodoListView()
        }
    }
}
