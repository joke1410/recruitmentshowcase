//
//  TodoListViewModel.swift
//

import Foundation
import Combine
import WatchCommunicationProtocol

final class TodoListViewModel: ObservableObject {
    
    @Published var todos: [WatchTodo] = []
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    @Injected<OptimisticAppStateManagerInterface> private var appStateManager
    
    init(todos: [WatchTodo] = []) {
        subscribeToAppStateChanges()
    }
}

private extension TodoListViewModel {
    
    func subscribeToAppStateChanges()  {
        appStateManager
            .appState
            .receive(on: DispatchQueue.main)
            .sink { [weak self] state in
                self?.todos = state.todos.sorted { $0.isDone && !$1.isDone }.reversed()
            }.store(in: &subscriptionBag)
    }
}

extension WatchTodo: Identifiable { }
