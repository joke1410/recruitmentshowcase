//
//  TodoDetailsView.swift
//

import SwiftUI

struct TodoDetailsView: View {
    
    @StateObject var viewModel: TodoDetailsViewModel
    
    init(todoId: String) {
        guard let vm = DependencyContainer.container?.resolve(
            TodoDetailsViewModel.self,
            argument: todoId)
        else {
            fatalError("Failed to resolve TodoDetailsViewModel")
        }
        _viewModel = StateObject(wrappedValue: vm)
    }
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 15) {
                Text(viewModel.title)
                    .foregroundColor(viewModel.isDone ? .gray : .white)
                    .font(.body)
                if let details = viewModel.details  {
                    Text(details)
                        .foregroundColor(viewModel.isDone ? .gray : .white)
                        .font(.footnote)
                }
                
                VStack {
                    Button("Mark as \(viewModel.isDone ? "NOT DONE" : "DONE")") {
                        viewModel.toggleDone()
                    }
                    Button("Delete") {
                        viewModel.remove()
                    }
                }
                Spacer()
            }
            .padding()
        }
    }
}

struct TodoDetailsView_Previews: PreviewProvider {
    
    static var previews: some View {
        TodoDetailsView(todoId: "id1")
    }
}
