//
//  TodoDetailsViewModel.swift
//

import Foundation
import Combine

final class TodoDetailsViewModel: ObservableObject {
    
    @Published var title: String = ""
    @Published var details: String?
    @Published var isDone: Bool = false
    
    private let todoId: String
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    @Injected<OptimisticAppStateManagerInterface> private var appStateManager
    @Injected<UserActionsProcessorInterface> private var actionsProcessor
    
    init(todoId: String) {
        self.todoId = todoId
        subscribeToAppStateChanges()
    }
    
    func toggleDone() {
        actionsProcessor.toggleDone(isDone: !isDone, todoId: todoId)
    }
    
    func remove() {
        actionsProcessor.remove(todoId: todoId)
    }
}

private extension TodoDetailsViewModel {
    func subscribeToAppStateChanges()  {
        appStateManager
            .appState
            .receive(on: DispatchQueue.main)
            .compactMap { $0.todos.first { $0.id ==  self.todoId }}
            .sink { [weak self] todo in
                self?.title = todo.title
                self?.details = todo.details
                self?.isDone = todo.isDone
            }.store(in: &subscriptionBag)
    }
}

