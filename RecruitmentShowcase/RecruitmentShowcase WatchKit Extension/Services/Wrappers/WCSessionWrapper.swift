//
//  WCSessionWrapper.swift
//

import Foundation
import WatchConnectivity
import Combine

final class WCSessionWrapper: NSObject & WCSessionWrapperInterface {
    
    var sessionIsReachable: AnyPublisher<Bool, Never> {
        sessionReachabilityChangedSubject.eraseToAnyPublisher()
    }
    
    var sessionActivationCompleted: AnyPublisher<Void, Never> {
        sessionActivationCompletedSubject.eraseToAnyPublisher()
    }
    
    var sessionApplicationContextReceived: AnyPublisher<Void, Never> {
        sessionApplicationContextReceivedSubject.eraseToAnyPublisher()
    }
    
    var isActive: Bool {
        session.activationState == .activated
    }
    
    var isSupported: Bool {
        session.isSupported
    }
    
    var receivedApplicationContext: [String: Any] {
        session.receivedApplicationContext
    }
    
    private var sessionReachabilityChangedSubject = CurrentValueSubject<Bool, Never>(false)
    private var sessionActivationCompletedSubject = PassthroughSubject<Void, Never>()
    private var sessionApplicationContextReceivedSubject = PassthroughSubject<Void, Never>()

    private let session: WCSessionInterface
    
    init(session: WCSessionInterface) {
        self.session = session
        super.init()
        self.session.delegate = self
    }
    
    func activate() {
        session.activate()
    }
    
    func sendMessage(_ message: [String : Any]) {
        sendMessage(message, replyHandler: nil, errorHandler: nil)
    }
    
    func sendMessage(_ message: [String : Any], replyHandler: (([String : Any]) -> Void)? = nil, errorHandler: ((Error) -> Void)? = nil) {
        session.sendMessage(message, replyHandler: replyHandler, errorHandler: errorHandler)
    }
}

protocol WCSessionWrapperInterface {
    var isSupported: Bool { get }
    var isActive: Bool { get }
    var receivedApplicationContext: [String: Any] { get }
    
    var sessionIsReachable: AnyPublisher<Bool, Never> { get }
    var sessionActivationCompleted: AnyPublisher<Void, Never> { get }
    var sessionApplicationContextReceived: AnyPublisher<Void, Never> { get }
    
    func activate()
    func sendMessage(_ message: [String : Any])
    func sendMessage(_ message: [String : Any], replyHandler: (([String : Any]) -> Void)?, errorHandler: ((Error) -> Void)?)
}

extension WCSessionWrapper: WCSessionDelegate {
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        sessionActivationCompletedSubject.send()
        sessionReachabilityChangedSubject.send(session.isReachable)
    }
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        sessionApplicationContextReceivedSubject.send()
    }
    
    func sessionReachabilityDidChange(_ session: WCSession) {
        sessionReachabilityChangedSubject.send(session.isReachable)
    }
}

protocol WCSessionInterface: AnyObject {
    var delegate: WCSessionDelegate? { get set }
    var isSupported: Bool { get }
    var activationState: WCSessionActivationState { get }
    var receivedApplicationContext: [String: Any] { get }
    
    func activate()
    func sendMessage(_ message: [String : Any], replyHandler: (([String : Any]) -> Void)?, errorHandler: ((Error) -> Void)?)
}

extension WCSession: WCSessionInterface {
    
    var isSupported: Bool {
        WCSession.isSupported()
    }
}

