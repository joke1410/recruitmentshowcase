//
//  ConnectivityService.swift
//

import Foundation
import WatchConnectivity
import Combine
import WatchCommunicationProtocol

protocol ConnectivityServiceInterface {
    
    var appState: AnyPublisher<AppState?, Never> { get }
    
    func start()
    func send(intent: Intent)
}

final class ConnectivityService: ConnectivityServiceInterface, Logging {
    
    var appState: AnyPublisher<AppState?, Never> {
        appStateSubject.eraseToAnyPublisher()
    }
    
    private let appStateSubject = CurrentValueSubject<AppState?, Never>(nil)
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    @Injected<WCSessionWrapperInterface> private var watchSession
    @Injected<CommunicationProtocolConverterInterface> private var converter
    
    init() {
        subscribeToSessionUpdates()
    }
    
    func start() {
        guard watchSession.isSupported else { return }
        
        watchSession.activate()
    }
    
    func send(intent: Intent) {
        guard let intentDictionaryMessage = converter.convertToIntentDictionaryMessage(intent: intent) else { return }
        
        watchSession.sendMessage(intentDictionaryMessage)
    }
}

private extension ConnectivityService {
    
    func handle(applicationContext: [String : Any]) {
        guard let appState = converter.convertToAppState(dictionaryMessage: applicationContext) else { return }
        appStateSubject.send(appState)
    }
    
    func subscribeToSessionUpdates() {
        Publishers.Merge3(
            watchSession.sessionActivationCompleted,
            watchSession.sessionApplicationContextReceived,
            watchSession.sessionIsReachable.flatMap { _ in Just<Void>(Void()) }
        ).sink { [weak self] in
            guard let self = self else { return }
            self.handle(applicationContext: self.watchSession.receivedApplicationContext)
        }.store(in: &subscriptionBag)
    }
}
