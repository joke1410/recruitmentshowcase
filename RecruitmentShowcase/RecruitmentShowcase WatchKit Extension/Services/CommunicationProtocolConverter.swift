//
//  CommunicationProtocolConverter.swift
//

import Foundation
import WatchCommunicationProtocol

protocol CommunicationProtocolConverterInterface {
    
    func convertToIntentDictionaryMessage(intent: Intent) -> [String: Any]?
    func convertToAppState(dictionaryMessage: [String: Any]) -> AppState?
}

final class CommunicationProtocolConverter: CommunicationProtocolConverterInterface, Logging {
    
    func convertToIntentDictionaryMessage(intent: Intent) -> [String: Any]? {
        guard let intentDictionaryMessage = intent.asDictionaryMessage else {
            log("Failed to convert intent \(type(of: intent)) to dictionary message.")
            return nil
        }
        return intentDictionaryMessage
    }
    
    func convertToAppState(dictionaryMessage: [String: Any]) -> AppState? {
        extract(
            AppState.self,
            from: dictionaryMessage,
            basedOnKey: CommunicationProtocolMainObjectKey.appState.rawValue
        )
    }
}

private extension CommunicationProtocolConverter {
    
    func extract<T: Decodable>(_ type: T.Type, from dictionary: [String: Any], basedOnKey key: String) -> T? {
        guard
            let data = dictionary[key] as? Data,
            let decoded = try? JSONDecoder().decode(T.self, from: data)
        else {
            log("Failed to convert dictionary key \(key) to type \(type).")
            return nil
        }
        
        return decoded
    }
}

private extension Intent {
    
    var asDictionaryMessage: [String: Any]? {
        guard let intent = try? JSONEncoder().encode(self) else { return nil }
        
        return [
            CommunicationProtocolMainObjectKey.intent.rawValue: intent,
            CommunicationProtocolMainObjectKey.intentType.rawValue: Self.type.rawValue
        ]
    }
}

