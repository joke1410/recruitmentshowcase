//
//  OptimisticAppStateManager.swift
//

import Foundation
import Combine
import WatchCommunicationProtocol

protocol OptimisticAppStateManagerInterface: AvailableUserActions {
    var appState: AnyPublisher<AppState, Never> { get }
}

final class OptimisticAppStateManager: OptimisticAppStateManagerInterface {
    
    var appState: AnyPublisher<AppState, Never> {
        appStateSubject
            .compactMap { $0 }
            .eraseToAnyPublisher()
    }
    
    private let appStateSubject = CurrentValueSubject<AppState?, Never>(nil)
    private var subscriptionBag = Set<AnyCancellable>()
    private var timer: Timer?
    @Injected<AppStateHandlerInterface> private var appStateHandler
    @Injected<IntentsQueueServiceInterface> private var queueService
    private let waitingIntervalInSeconds: TimeInterval
    
    init(waitingIntervalInSeconds: TimeInterval = 5) {
        self.waitingIntervalInSeconds = waitingIntervalInSeconds
        setupSubscriptions()
    }
    
    func toggleDone(isDone: Bool, todoId: String) {
        handleToggleDone(isDone: isDone, todoId: todoId)
    }
    
    func remove(todoId: String) {
        handleRemove(todoId: todoId)
    }
}

private extension OptimisticAppStateManager {
    
    func setupSubscriptions() {
        
        appStateHandler
            .appState
            .filter { [weak self] _ in
                self?.queueService.lastQueueState == .isEmpty
            }.sink { [weak self] state in
                self?.appStateSubject.send(state)
            }.store(in: &subscriptionBag)
        
        queueService
            .queueState
            .removeDuplicates()
            .sink { [weak self] state in
                switch state {
                case .isEmpty: self?.setupWaitingTimer()
                case .processing: self?.destroyWaitingTimer()
                }
            }.store(in: &subscriptionBag)
    }
    
    func setupWaitingTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: waitingIntervalInSeconds, repeats: false) { [weak self] _ in
            self?.applyLastAppState()
            self?.destroyWaitingTimer()
        }
    }
    
    func destroyWaitingTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    func applyLastAppState() {
        guard let state = appStateHandler.lastAppState else { return }
        appStateSubject.send(state)
    }
    
    func handleToggleDone(isDone: Bool, todoId: String) {
        guard
            var state = appStateSubject.value,
            let index = state.todos.firstIndex(where: {  $0.id == todoId })
        else { return }
        
        state.todos[index].isDone = isDone
        appStateSubject.send(state)
    }
    
    func handleRemove(todoId: String) {
        guard var state = appStateSubject.value else { return }
        state.todos.removeAll { $0.id == todoId }
        appStateSubject.send(state)
    }
}
