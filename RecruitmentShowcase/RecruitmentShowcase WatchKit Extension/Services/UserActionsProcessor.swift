//
//  UserActionsProcessor.swift
//

import Foundation
import WatchCommunicationProtocol

protocol AvailableUserActions {
    func toggleDone(isDone: Bool, todoId: String)
    func remove(todoId: String)
}

protocol UserActionsProcessorInterface: AvailableUserActions { }

final class UserActionsProcessor: UserActionsProcessorInterface {
    
    @Injected<IntentsQueueServiceInterface> private var queueService
    @Injected<OptimisticAppStateManagerInterface> private var optimisticAppStateManager
    
    func toggleDone(isDone: Bool, todoId: String) {
        optimisticAppStateManager.toggleDone(isDone: isDone, todoId: todoId)
        
        let intent = ToggleDoneIntent(todoId: todoId, isDone: isDone)
        queueService.queue(intent: intent)
    }
    
    func remove(todoId: String) {
        optimisticAppStateManager.remove(todoId: todoId)
        
        let intent = DeleteIntent(todoId: todoId)
        queueService.queue(intent: intent)
    }
}
