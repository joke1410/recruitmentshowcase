//
//  WatchStateSevice.swift
//

import Foundation
import Combine

protocol WatchStateServiceInterface {
    var isWatchSessionReachable: AnyPublisher<Bool, Never> { get }
}

final class WatchStateSevice: WatchStateServiceInterface {
    
    var isWatchSessionReachable: AnyPublisher<Bool, Never> {
        watchSession.sessionIsReachable
    }
    
    @Injected<WCSessionWrapperInterface> private var watchSession
}
