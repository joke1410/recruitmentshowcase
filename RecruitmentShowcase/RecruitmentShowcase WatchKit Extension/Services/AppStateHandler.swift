//
//  AppStateHandler.swift
//

import Foundation
import Combine
import WatchCommunicationProtocol

protocol AppStateHandlerInterface {
    var appState: AnyPublisher<AppState, Never> { get }
    var lastAppState: AppState? { get }
}

final class AppStateHandler: AppStateHandlerInterface {
    
    var lastAppState: AppState?
    
    var appState: AnyPublisher<AppState, Never> {
        connectivityService
            .appState
            .compactMap { [weak self] in
                if let state = $0 { self?.lastAppState = state }
                return $0
            }
            .eraseToAnyPublisher()
    }
    
    @Injected<ConnectivityServiceInterface> private var connectivityService
}
