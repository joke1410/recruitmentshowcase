//
//  IntentsQueueService.swift
//

import Foundation
import Combine
import WatchCommunicationProtocol

enum IntentsQueueState {
    case isEmpty, processing
}

protocol IntentsQueueServiceInterface {
    var queueState: AnyPublisher<IntentsQueueState, Never> { get }
    var lastQueueState: IntentsQueueState { get }
    func queue(intent: Intent)
}

final class IntentsQueueService: IntentsQueueServiceInterface {
    
    var queueState: AnyPublisher<IntentsQueueState, Never> { queueStateSubject.eraseToAnyPublisher() }
    var lastQueueState: IntentsQueueState { queueStateSubject.value }
    
    @Injected<WatchStateServiceInterface> private var watchState
    @Injected<ConnectivityServiceInterface> private var connectivityService
    private var queueStateSubject = CurrentValueSubject<IntentsQueueState, Never>(.isEmpty)
    private var waitingIntentsArray: [Intent] = []
    private var timerPublisher: Publishers.Autoconnect<Timer.TimerPublisher>?
    private var timerSubscription: AnyCancellable?
    private var subscriptionBag = Set<AnyCancellable>()
    private let sendingIntervalInSeconds: Double
    private let intentExpiringTimeInSeconds: Double
    
    init(
        sendingIntervalInSeconds: Double = 0.25,
        intentExpiringTimeInSeconds: Double = 5
    ) {
        self.sendingIntervalInSeconds = sendingIntervalInSeconds
        self.intentExpiringTimeInSeconds = intentExpiringTimeInSeconds
        
        subscribeToSessionReachabilityChange()
    }
    
    func queue(intent: Intent) {
        queueStateSubject.send(.processing)
        
        waitingIntentsArray.removeAll(where: { $0.timestamp.timeIntervalSinceNow <= -intentExpiringTimeInSeconds })
        
        waitingIntentsArray.append(intent)
                                           
        if timerPublisher == nil { setupSendingTimer() }
    }
}

private extension IntentsQueueService {
    
    func setupSendingTimer() {
        timerPublisher = Timer
            .publish(every: sendingIntervalInSeconds, tolerance: nil, on: .main, in: .common, options: nil)
            .autoconnect()
        timerSubscription = timerPublisher?.sink { [weak self] _ in
            self?.handleTimerEvent()
        }
        timerSubscription?.store(in: &subscriptionBag)
    }
    
    func handleTimerEvent() {
        
        guard let intentToBeSend = waitingIntentsArray.first else { return }

        waitingIntentsArray.removeFirst()
        connectivityService.send(intent: intentToBeSend)

        if waitingIntentsArray.isEmpty {
            cleanupTimer()
            queueStateSubject.send(.isEmpty)
        }
    }
    
    func subscribeToSessionReachabilityChange() {
        watchState
            .isWatchSessionReachable
            .sink { [weak self] isReachable in
                if !isReachable {
                    self?.cleanupService()
                }
            }
            .store(in: &subscriptionBag)
    }
    
    func cleanupTimer() {
        timerSubscription?.cancel()
        timerPublisher = nil
    }
    
    func cleanupService() {
        cleanupTimer()
        waitingIntentsArray.removeAll()
    }
}
