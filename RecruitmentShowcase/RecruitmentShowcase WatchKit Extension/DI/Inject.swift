//
//  Inject.swift
//

import Foundation
import SwiftUI

@propertyWrapper struct Injected<Value> {
    var wrappedValue: Value {
        guard let value = DependencyContainer.resolver.resolve(Value.self) else {
            fatalError("Cannot inject unregistered object of type \(Value.self)")
        }
        return value
    }
}

@propertyWrapper struct InjectedObservedObject<Value>: DynamicProperty where Value: ObservableObject {

    @ObservedObject private var value: Value
    
    var wrappedValue: Value {
        get { return value }
        mutating set { value = newValue }
    }
    
    var projectedValue: ObservedObject<Value>.Wrapper {
        return self.$value
    }
        
    init() {
        guard let resolvedValue = DependencyContainer.resolver.resolve(Value.self) else {
            fatalError("Cannot inject unregistered object of type \(Value.self)")
        }
        self.value = resolvedValue
    }
}
