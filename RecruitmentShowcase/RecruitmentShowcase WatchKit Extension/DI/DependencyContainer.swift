//
//  DependencyContainer.swift
//

import Swinject
import WatchConnectivity

final class DependencyContainer {
    
    static private(set) var container: Container?
    
    static var resolver: Resolver {
        guard let resolver = container else {
            fatalError("Build container before trying to access resolver")
        }
        return resolver
    }
    
    static func build() {
        container = Container()
        
        #if InUnitTests
        // The services are not registered for Unit Tests so that we have isolated container
        // in which we register mocks as dependencies for particular services in unit tests.
        #else
        registerServices()
        #endif
    }
    
    private static func registerServices() {
        
        container?.register(Logger.self, factory: { _ in
            Logger()
        }).inObjectScope(.container)
        
        container?.register(WCSessionWrapperInterface.self, factory: { _ in
            WCSessionWrapper(session: WCSession.default)
        }).inObjectScope(.container)
        
        container?.register(CommunicationProtocolConverterInterface.self, factory: { _ in
            CommunicationProtocolConverter()
        }).inObjectScope(.container)
        
        container?.register(ConnectivityServiceInterface.self, factory: { _ in
            ConnectivityService()
        }).inObjectScope(.container)
        
        container?.register(AppStateHandlerInterface.self, factory: { _ in
            AppStateHandler()
        }).inObjectScope(.container)
        
        container?.register(OptimisticAppStateManagerInterface.self, factory: { _ in
            OptimisticAppStateManager()
        }).inObjectScope(.container)
        
        container?.register(IntentsQueueServiceInterface.self, factory: { _ in
            IntentsQueueService()
        }).inObjectScope(.container)
        
        container?.register(UserActionsProcessorInterface.self, factory: { _ in
            UserActionsProcessor()
        }).inObjectScope(.container)
        
        container?.register(WatchStateServiceInterface.self, factory: { _ in
            WatchStateSevice()
        }).inObjectScope(.container)
    
        container?.register(ContentViewModel.self) { _ in
            ContentViewModel()
        }.inObjectScope(.container)
        
        container?.register(TodoListViewModel.self) { _ in
            TodoListViewModel()
        }.inObjectScope(.container)
        
        container?.register(TodoDetailsViewModel.self) { (r, todoId: String) in
            TodoDetailsViewModel(todoId: todoId)
        }
    }
}
