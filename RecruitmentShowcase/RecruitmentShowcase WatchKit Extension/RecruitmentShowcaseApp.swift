//
//  RecruitmentShowcaseApp.swift
//

import SwiftUI

@main
struct RecruitmentShowcaseApp: App {
    
    @Injected<ConnectivityServiceInterface> private var connectivityService
    
    init() {
        DependencyContainer.build()
        
        startServices()
    }
    
    var body: some Scene {
        WindowGroup {
            baseView
        }
    }
}

private extension RecruitmentShowcaseApp {
    
    var baseView: some View {
        #if InUnitTests
        EmptyView()
        #else
        NavigationView {
            ContentView()
        }
        #endif
    }
    
    func startServices() {
        #if InUnitTests
        // Services should be started only for actual implementation.
        // Do not start services for unit tests.
        #else
        connectivityService.start()
        #endif
    }
}
