//
//  OptimisticAppStateManagerTests.swift
//  RecruitmentShowcase WatchKit AppTests
//

import XCTest
import Combine
import Swinject
import WatchCommunicationProtocol
@testable import RecruitmentShowcase_WatchKit_Extension

final class OptimisticAppStateManagerTests: XCTestCase {
    
    private let container = DependencyContainer.container
    
    var sut: OptimisticAppStateManager!
    var queueService: IntentsQueueServiceMock!
    var appStateHandler: AppStateHandlerMock!
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    override func setUp() {
        queueService = IntentsQueueServiceMock()
        appStateHandler = AppStateHandlerMock()
        container?.register(IntentsQueueServiceInterface.self) { _ in
            self.queueService
        }
        container?.register(AppStateHandlerInterface.self) { _ in
            self.appStateHandler
        }
        
        sut = OptimisticAppStateManager(waitingIntervalInSeconds: 2)
    }
    
    override func tearDown() {
        container?.removeAll()
        queueService = nil
        appStateHandler = nil
        subscriptionBag.removeAll()
        sut = nil
        super.tearDown()
    }
    
    func testAppStateIsPassedFromAppStateHandlerWhenQueueIsEmpty() {
        let exp = expectation(description: "AppState updated")
        let mockAppState = AppState(timestamp: Date(), todos: [])
        
        sut.appState.sink { state in
            if state.timestamp == mockAppState.timestamp {
                exp.fulfill()
            }
            
        }.store(in: &subscriptionBag)
        
        queueService.lastQueueState = .isEmpty
        appStateHandler.appStateSubject.send(mockAppState)
        
        waitForExpectations(timeout: 2)
    }
    
    func testAppStateIsNotPassedFromAppStateHandlerWhenQueueIsProcessing() {
        
        let mockAppState = AppState(timestamp: Date(), todos: [])
        
        var appState: AppState?
        
        sut.appState.sink { state in
            appState = state
        }.store(in: &subscriptionBag)
        
        queueService.lastQueueState = .processing
        appStateHandler.appStateSubject.send(mockAppState)
        
        verifyAfter(seconds: 2) {
            XCTAssertNil(appState)
        }
    }
    
    func testAppStateIsNotPassedFromAppStateHandlerWhenQueueFinishProcessingAndRequiredWaitingTimeHasNotPassed() {
        
        let mockAppState = AppState(timestamp: Date(), todos: [])
        
        appStateHandler.lastAppState = mockAppState
        queueService.queueStateSubject.send(.processing)
        
        var appState: AppState?
        
        sut.appState.sink { state in
            appState = state
        }.store(in: &subscriptionBag)
        
        queueService.queueStateSubject.send(.isEmpty)
        
        verifyAfter(seconds: 1) {
            XCTAssertNil(appState)
        }
    }
    
    func testAppStateIsPassedFromAppStateHandlerWhenQueueFinishProcessingAndRequiredWaitingTimeHasPassed() {
        
        let mockAppState = AppState(timestamp: Date(), todos: [])
        
        appStateHandler.lastAppState = mockAppState
        queueService.queueStateSubject.send(.processing)
        
        var appState: AppState?
        
        sut.appState.sink { state in
            appState = state
        }.store(in: &subscriptionBag)
        
        queueService.queueStateSubject.send(.isEmpty)
        
        verifyAfter(seconds: 3) {
            XCTAssert(appState?.timestamp == mockAppState.timestamp)
        }
    }
    
    func testAppStateUpdatedAfterToggleDoneActionComesFromUser() {
        var appState: AppState?
        
        sut.appState.sink { state in
            appState = state
        }.store(in: &subscriptionBag)
        
        queueService.lastQueueState = .isEmpty
        appStateHandler.appStateSubject.send(.initialAppState)
        let idOfUpdatedTodo = "1"
        sut.toggleDone(isDone: true, todoId: idOfUpdatedTodo)
        
        verifyAfter(seconds: 1) {
            XCTAssertTrue(appState?.todos.first{ $0.id == idOfUpdatedTodo }?.isDone == true)
        }
    }
    
    func testAppStateUpdatedAfterRemoteMicDisableActionComesFromUser() {
        var appState: AppState?
        
        sut.appState.sink { state in
            appState = state
        }.store(in: &subscriptionBag)
        
        queueService.lastQueueState = .isEmpty
        appStateHandler.appStateSubject.send(.initialAppState)
        
        let idToRemove = "1"
        sut.remove(todoId: idToRemove)
        
        verifyAfter(seconds: 1) {
            XCTAssertTrue(appState?.todos.filter { $0.id == idToRemove }.isEmpty == true)
        }
    }
}

private extension AppState {
    
    static var initialAppState: AppState {
        AppState(
            todos: [
                .init(id: "1", title: "1", details: "1", isDone: false),
                .init(id: "2", title: "2", details: "2", isDone: false),
                .init(id: "3", title: "3", details: "3", isDone: false)
            ]
        )
    }
}
