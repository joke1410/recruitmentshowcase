//
//  UserActionsProcessorTests.swift
//

import XCTest
import Swinject
import WatchCommunicationProtocol
@testable import RecruitmentShowcase_WatchKit_Extension

final class UserActionsProcessorTests: XCTestCase {
    
    private let container = DependencyContainer.container
    
    var sut: UserActionsProcessor!
    var queueService: IntentsQueueServiceMock!
    var optimisticAppStateManager: OptimisticAppStateManagerMock!
    
    override func setUp() {
        queueService = IntentsQueueServiceMock()
        optimisticAppStateManager = OptimisticAppStateManagerMock()
        container?.register(IntentsQueueServiceInterface.self) { _ in
            self.queueService
        }
        container?.register(OptimisticAppStateManagerInterface.self) { _ in
            self.optimisticAppStateManager
        }
        
        sut = UserActionsProcessor()
    }
    
    override func tearDown() {
        container?.removeAll()
        queueService = nil
        optimisticAppStateManager = nil
        sut = nil
        super.tearDown()
    }
    
    func testAllIntentsQueued() {
        
        // toggle done
        testIntentQueued(expectedType: .toggleDone) {
            sut.toggleDone(isDone: true, todoId: "1")
        }
        
        // remove
        testIntentQueued(expectedType: .delete) {
            sut.remove(todoId: "1")
        }
    }
    
    func testProperOptimisticAppStateMethodsCalled() {
        
        // toggle done
        callVerifier { called in
            optimisticAppStateManager.toggleDoneFuncStub = { _ in called() }
        } afterAction: {
            sut.toggleDone(isDone: true, todoId: "1")
        }
        
        // remove
        callVerifier { called in
            optimisticAppStateManager.removeFuncStub = { _ in called() }
        } afterAction: {
            sut.remove(todoId: "1")
        }
    }
    
    private func testIntentQueued(expectedType: IntentType, action: () -> Void) {
        
        var queuedIntent: Intent?
        
        queueService.queueFuncStub = { queuedIntent = $0 }
        
        action()
        
        XCTAssert(queuedIntent?.type == expectedType)
    }
    
    private func callVerifier(verifyCall: (_ called: @escaping () -> Void) -> Void, afterAction action: () -> Void) {
        let exp = expectation(description: "Method called")
        var methodCalled = false
        
        verifyCall {
            methodCalled = true
            exp.fulfill()
        }
        
        action()
        
        wait(for: [exp], timeout: 1)
        
        XCTAssertTrue(methodCalled)
    }
}
