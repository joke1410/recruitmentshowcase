//
//  Helpers.swift
//  RecruitmentShowcase WatchKit AppTests
//

import Foundation
import XCTest

func funcStub<I,O>(of: (I) -> (O), returns: O) -> (I) -> (O){
    return { _ in returns }
}

extension XCTestCase {
    func verifyAfter(seconds: TimeInterval, block: () -> Void) {
        let exp = expectation(description: "Test after \(seconds) seconds")
        let result = XCTWaiter.wait(for: [exp], timeout: seconds)
        if result == XCTWaiter.Result.timedOut {
            block()
        } else {
            XCTFail("Something went wrong")
        }
    }
}
