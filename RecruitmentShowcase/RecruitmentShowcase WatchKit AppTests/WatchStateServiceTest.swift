//
//  WatchStateServiceTest.swift
//

import XCTest
import Swinject
import Combine
import WatchConnectivity
@testable import RecruitmentShowcase_WatchKit_Extension

final class WatchStateServiceTest: XCTestCase {
    
    private let container = DependencyContainer.container
    
    var sut: WatchStateSevice!
    var watchSessionWrapper: WCSessionWrapperMock!
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    override func setUp() {
        watchSessionWrapper = WCSessionWrapperMock()
        
        container?.register(WCSessionWrapperInterface.self) { _ in
            self.watchSessionWrapper
        }
        
        sut = WatchStateSevice()
    }
    
    override func tearDown() {
        container?.removeAll()
        subscriptionBag.removeAll()
        watchSessionWrapper = nil
        super.tearDown()
    }
    
    func testSessionIsReachable() {
        
        var isReachable: Bool?
        
        sut.isWatchSessionReachable.sink(receiveValue: { isReachable = $0 }).store(in: &subscriptionBag)
        watchSessionWrapper.sessionIsReachableSubject.send(true)
        
        XCTAssertTrue(isReachable == true)
    }
}
