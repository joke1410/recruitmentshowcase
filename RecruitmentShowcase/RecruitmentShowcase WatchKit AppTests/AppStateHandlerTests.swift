//
//  AppStateHandlerTests.swift
//

import XCTest
import Swinject
import Combine
import WatchCommunicationProtocol
@testable import RecruitmentShowcase_WatchKit_Extension

final class AppStateHandlerTests: XCTestCase {
    
    private let container = DependencyContainer.container
    
    var sut: AppStateHandler!
    var connectivityService: ConnectivityServiceMock!
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    override func setUp() {
        connectivityService = ConnectivityServiceMock()
        container?.register(ConnectivityServiceInterface.self) { _ in
            self.connectivityService
        }
        
        sut = AppStateHandler()
    }
    
    override func tearDown() {
        container?.removeAll()
        subscriptionBag.removeAll()
        connectivityService = nil
        sut = nil
        super.tearDown()
    }
    
    func testAppStateUpdated() {
        
        let mockAppState = AppState(timestamp: Date(), todos: [])
        
        var newState: AppState?
        let expectation = expectation(description: "State updated")
        
        sut.appState.sink { state in
            newState = state
            expectation.fulfill()
        }.store(in: &subscriptionBag)
        
        connectivityService.appStateSubject.send(mockAppState)
        
        waitForExpectations(timeout: 3)
        
        XCTAssertTrue(newState?.timestamp == mockAppState.timestamp)
        XCTAssertTrue(sut.lastAppState?.timestamp == mockAppState.timestamp)
    }
}
