//
//  WCSessionWrapperMock.swift
//

import Foundation
import Combine
@testable import RecruitmentShowcase_WatchKit_Extension

final class WCSessionWrapperMock: WCSessionWrapperInterface {
    
    var isSupported: Bool = false
    
    var isActive: Bool = false
    
    var receivedApplicationContext: [String : Any] = [:]
    
    var sessionIsReachableSubject = PassthroughSubject<Bool, Never>()
    var sessionIsReachable: AnyPublisher<Bool, Never> {
        sessionIsReachableSubject.eraseToAnyPublisher()
    }
    
    var sessionActivationCompletedSubject = PassthroughSubject<Void, Never>()
    var sessionActivationCompleted: AnyPublisher<Void, Never> {
        sessionActivationCompletedSubject.eraseToAnyPublisher()
    }
    
    var sessionApplicationContextReceivedSubject = PassthroughSubject<Void, Never>()
    var sessionApplicationContextReceived: AnyPublisher<Void, Never> {
        sessionApplicationContextReceivedSubject.eraseToAnyPublisher()
    }
    
    lazy var activateFuncStub = funcStub(of: activate, returns: ())
    func activate() {
        activateFuncStub(())
    }
    
    lazy var sendMessageFuncStub = funcStub(of: sendMessage, returns: ())
    func sendMessage(_ message: [String : Any]) {
        sendMessageFuncStub(message)
    }
    
    lazy var sendMessageWithReplyFuncStub = funcStub(of: sendMessage(_:replyHandler:errorHandler:), returns: ())
    func sendMessage(_ message: [String : Any], replyHandler: (([String : Any]) -> Void)?, errorHandler: ((Error) -> Void)?) {
        sendMessageWithReplyFuncStub((message, replyHandler, errorHandler))
    }
}
