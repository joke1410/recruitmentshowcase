//
//  IntentsQueueServiceMock.swift
//

import Foundation
import Combine
import WatchCommunicationProtocol
@testable import RecruitmentShowcase_WatchKit_Extension

final class IntentsQueueServiceMock: IntentsQueueServiceInterface {
    
    var queueState: AnyPublisher<IntentsQueueState, Never> { return queueStateSubject.eraseToAnyPublisher() }
    var queueStateSubject = PassthroughSubject<IntentsQueueState, Never>()
    
    var lastQueueState: IntentsQueueState = .isEmpty
    
    lazy var queueFuncStub = funcStub(of: queue(intent:), returns: ())
    func queue(intent: Intent) {
        queueFuncStub(intent)
    }
}
