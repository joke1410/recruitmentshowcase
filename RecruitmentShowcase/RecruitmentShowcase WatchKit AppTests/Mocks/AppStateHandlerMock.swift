//
//  AppStateHandlerMock.swift

import Foundation
import Combine
import WatchCommunicationProtocol
@testable import RecruitmentShowcase_WatchKit_Extension

final class AppStateHandlerMock: AppStateHandlerInterface {
    
    var lastAppState: AppState?
    
    var appStateSubject = PassthroughSubject<AppState, Never>()
    var appState: AnyPublisher<AppState, Never> {
        appStateSubject.eraseToAnyPublisher()
    }
}
