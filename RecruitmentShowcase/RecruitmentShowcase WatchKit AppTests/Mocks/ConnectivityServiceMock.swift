//
//  ConnectivityServiceMock.swift
//

import Foundation
import Combine
import WatchCommunicationProtocol
@testable import RecruitmentShowcase_WatchKit_Extension

final class ConnectivityServiceMock: ConnectivityServiceInterface {
    
    var appState: AnyPublisher<AppState?, Never> {
        appStateSubject.eraseToAnyPublisher()
    }
    var appStateSubject = PassthroughSubject<AppState?, Never>()
    
    lazy var startFuncStub = funcStub(of: start, returns: ())
    func start() {
        startFuncStub(())
    }
    
    lazy var sendIntentFuncStub = funcStub(of: send, returns: ())
    func send(intent: Intent) {
        sendIntentFuncStub(intent)
    }
}
