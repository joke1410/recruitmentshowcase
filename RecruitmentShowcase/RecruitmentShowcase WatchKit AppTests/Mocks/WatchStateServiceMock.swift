//
//  WatchStateServiceMock.swift
//

import Foundation
import Combine
@testable import RecruitmentShowcase_WatchKit_Extension

final class WatchStateSeviceMock: WatchStateServiceInterface {
    
    var isWatchSessionReachableSubject = PassthroughSubject<Bool, Never>()
    var isWatchSessionReachable: AnyPublisher<Bool, Never> {
        isWatchSessionReachableSubject.eraseToAnyPublisher()
    }
}
