//
//  OptimisticAppStateManagerMock.swift
//

import Foundation
import Combine
import WatchCommunicationProtocol
@testable import RecruitmentShowcase_WatchKit_Extension

final class OptimisticAppStateManagerMock: OptimisticAppStateManagerInterface {
    
    var appStateSubject = PassthroughSubject<AppState, Never>()
    var appState: AnyPublisher<AppState, Never> {
        appStateSubject.eraseToAnyPublisher()
    }
    
    lazy var toggleDoneFuncStub = funcStub(of: toggleDone, returns: ())
    func toggleDone(isDone: Bool, todoId: String) {
        toggleDoneFuncStub((isDone, todoId))
    }
    
    lazy var removeFuncStub = funcStub(of: remove, returns: ())
    func remove(todoId: String) {
        removeFuncStub(todoId)
    }
}
