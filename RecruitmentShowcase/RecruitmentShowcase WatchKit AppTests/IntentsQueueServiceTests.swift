//
//  IntentsQueueServiceTests.swift
//

import XCTest
import Swinject
import Combine
import WatchCommunicationProtocol
@testable import RecruitmentShowcase_WatchKit_Extension

final class IntentsQueueServiceTests: XCTestCase {
    
    private let container = DependencyContainer.container
    
    var sut: IntentsQueueService!
    var watchStateService: WatchStateSeviceMock!
    var connectivityService: ConnectivityServiceMock!
    var sendingInterval: Double = 1
    var expiringTime: Double = 0.5
    
    private var subsciptionBag = Set<AnyCancellable>()
    
    override func setUp() {
        watchStateService = WatchStateSeviceMock()
        connectivityService = ConnectivityServiceMock()
        
        container?.register(WatchStateServiceInterface.self) { _ in
            self.watchStateService
        }
        container?.register(ConnectivityServiceInterface.self) { _ in
            self.connectivityService
        }
        
        sut = IntentsQueueService(
            sendingIntervalInSeconds: sendingInterval,
            intentExpiringTimeInSeconds: expiringTime
        )
    }
    
    override func tearDown() {
        container?.removeAll()
        watchStateService = nil
        subsciptionBag.removeAll()
        sut = nil
        super.tearDown()
    }
    
    func testQueueIsCleanedIfSessionReachabilityDidChange() {
        var counter = 0
        let expectation = expectation(description: "Test if queue is cleaned when session reachability did change")
        
        let intent1 = ToggleDoneIntent(todoId: "1", isDone: true)
        let intent2 = DeleteIntent(todoId: "2")
        
        sut.queue(intent: intent1)
        sut.queue(intent: intent2)
        
        connectivityService.sendIntentFuncStub = { _ in
            counter += 1
        }
        
        watchStateService.isWatchSessionReachableSubject.send(false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { expectation.fulfill() }
        
        waitForExpectations(timeout: 2)
        XCTAssertTrue(counter == 0)
    }
    
    func testServiceIsRemovingExpiredIntentsFromQueue() {
        var counter = 0
        let expectation = expectation(description: "Test if expired events are removed from queue")
        
        sut.queue(intent: ToggleDoneIntent(todoId: "1", isDone: true))
        sut.queue(intent: DeleteIntent(todoId: "2"))
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.sut.queue(intent: DeleteIntent(todoId: "3"))
        }
        
        connectivityService.sendIntentFuncStub = { _ in
            counter += 1
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { expectation.fulfill() }
        
        waitForExpectations(timeout: 5)
        XCTAssertTrue(counter == 2)
    }
    
    func testQueueInitialStateIsEmpty() {
        var state: IntentsQueueState = .processing
        let expectation = expectation(description: "Test queue initial state isEmpty")
        
        sut.queueState.sink { queueState in
            state = queueState
            expectation.fulfill()
        }
        .store(in: &subsciptionBag)
        
        waitForExpectations(timeout: 1)
        XCTAssertTrue(state == .isEmpty)
        XCTAssertTrue(sut.lastQueueState == .isEmpty)
    }
    
    func testQueueStateChangeAfterIntentAddedToQueue() {
        var state: IntentsQueueState = .isEmpty
        
        let expectation = expectation(description: "Test queue state change after intent adding")
        
        sut.queue(intent: ToggleDoneIntent(todoId: "1", isDone: true))
        sut.queueState.sink { queueState in
            state = queueState
            expectation.fulfill()
        }
        .store(in: &subsciptionBag)
        
        waitForExpectations(timeout: 2)
        XCTAssertTrue(state == .processing)
        XCTAssertTrue(sut.lastQueueState == .processing)
    }
}
