//
//  DI.swift
//

import Swinject
import WatchConnectivity
import Networking
import BackgroundTasks

final class DI {
    
    static private(set) var container: Container?
    
    static var resolver: Resolver {
        guard let resolver = container else {
            fatalError("Build container before trying to access resolver")
        }
        return resolver
    }
    
    static func build() {
        container = Container()
        
        #if InUnitTests
        // The services are not registered for Unit Tests so that we have isolated container
        // in which we register mocks as dependencies for particular services in unit tests.
        #else
        registerServices()
        #endif
    }
    
    private static func registerServices() {
        
        container?.register(Logger.self) { r in
            Logger()
        }
        
        container?.register(URLSessionInterface.self) { r in
            URLSession.shared
        }
        
        container?.register(NetworkClientInterface.self) { r in
            NetworkClient(session: r.resolve(URLSessionInterface.self)!)
        }.inObjectScope(.container)
        
        container?.register(ServerEndpointsProviderInterface.self) { _ in
            ServerEndpointsProvider()
        }
        
        container?.register(TodosNetworkClientInterface.self) { r in
            TodosNetworkClient(
                networkClient: r.resolve(NetworkClientInterface.self)!,
                endpointsProvider: r.resolve(ServerEndpointsProviderInterface.self)!
            )
        }.inObjectScope(.container)
        
        container?.register(TodosRealmStorageInterface.self) { r in
            TodosRealmStorage(realmContainer: RealmDatabase(configuration: TodosRealmDatabaseConfiguration()))
        }.inObjectScope(.container)
        
        container?.register(SyncingServiceInterface.self) { r in
            SyncingService(
                networkClient: r.resolve(TodosNetworkClientInterface.self)!,
                storage: r.resolve(TodosRealmStorageInterface.self)!,
                dispatcher: AsyncDispatcher(
                    queue: DispatchQueue(label: "SyncingServiceQueue", qos: .background)
                )
            )
        }.inObjectScope(.container)
        
        container?.register(TodosServiceInterface.self) { r in
            TodosService(
                storage: r.resolve(TodosRealmStorageInterface.self)!,
                syncingService: r.resolve(SyncingServiceInterface.self)!,
                dispatcher: AsyncDispatcher(
                    queue: DispatchQueue(label: "TodosServiceQueue", qos: .background)
                )
            )
        }.inObjectScope(.container)
        
        container?.register(LocalPreferencesManagerInterface.self) { _ in
            LocalPreferencesManager(keyValueStorage: UserDefaults.standard)
        }.inObjectScope(.container)
        
        
        // Watch service
        
        let watchServiceDispatcher = AsyncDispatcher(
            queue: DispatchQueue(label: "WatchAppServiceQueue", qos: .background)
        )
        
        container?.register(WatchIntentsHandlerInterface.self) { r in
            WatchIntentsHandler(
                todosService: r.resolve(TodosServiceInterface.self)!,
                dispatcher: watchServiceDispatcher
            )
        }.inObjectScope(.container)
        
        container?.register(WCSessionWrapperInterface.self) { _ in
            WCSessionWrapper(session: WCSession.default)
        }.inObjectScope(.container)
        
        container?.register(WatchConnectivityServiceInterface.self) { r in
            WatchConnectivityService(
                session: r.resolve(WCSessionWrapperInterface.self)!,
                intentsHandler: r.resolve(WatchIntentsHandlerInterface.self)!
            )
        }.inObjectScope(.container)
        
        container?.register(WatchAppServiceInterface.self) { r in
            WatchAppService(
                connectivityService: r.resolve(WatchConnectivityServiceInterface.self)!,
                todosService: r.resolve(TodosServiceInterface.self)!,
                dispatcher: watchServiceDispatcher,
                asyncScheduler: watchServiceDispatcher.scheduler
            )
        }.inObjectScope(.container)
        
        container?.register(BackgroundTasksServiceInterface.self) { r in
            BackgroundTasksService(
                taskScheduler: BGTaskScheduler.shared,
                syncingService: r.resolve(SyncingServiceInterface.self)!
            )
        }.inObjectScope(.container)
    }
}
