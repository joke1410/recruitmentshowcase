//
//  ServerEndpointsProvider.swift
//

import Foundation

protocol ServerEndpointsProviderInterface {
    var todos: String { get }
}

struct ServerEndpointsProvider: ServerEndpointsProviderInterface {
    
    let todos: String = "https://pb-rc.herokuapp.com/api/v2/todos"
}
