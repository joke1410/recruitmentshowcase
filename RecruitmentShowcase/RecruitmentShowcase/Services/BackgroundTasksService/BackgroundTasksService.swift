//
//  BackgroundTasksService.swift
//

import Foundation
import BackgroundTasks
import Combine

protocol BackgroundTasksServiceInterface {
    func registerBackgroundTasks()
    func scheduleBackgroundTasks()
}

final class BackgroundTasksService: BackgroundTasksServiceInterface, Logging {
    
    private let appRefreshBackgroundTaskIdentifier = "com.pbruz.background.appRefresh"
    private let processingBackgroundTaskIdentifier = "com.pbruz.background.processing"
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    private let taskScheduler: BGTaskSchedulerInterface
    private let syncingService: SyncingServiceInterface
    private let backgroundFetchMinimumTimeInterval: TimeInterval
    
    init(taskScheduler: BGTaskSchedulerInterface,
         syncingService: SyncingServiceInterface,
         backgroundFetchMinimumTimeInterval: TimeInterval = 60 * 10) {
        self.taskScheduler = taskScheduler
        self.syncingService = syncingService
        self.backgroundFetchMinimumTimeInterval = backgroundFetchMinimumTimeInterval
    }
    
    func registerBackgroundTasks() {
        log("[BackgroundTasksService] Registering background tasks")
        
        taskScheduler.register(forTaskWithIdentifier: appRefreshBackgroundTaskIdentifier, using: .global()) { [weak self] task in
            guard let appRefreshTask = task as? BGAppRefreshTask else { return }
            self?.handle(task: appRefreshTask, type: .appRefresh)
        }
        
        taskScheduler.register(forTaskWithIdentifier: processingBackgroundTaskIdentifier, using: .global()) { [weak self] task in
            guard let processingTask = task as? BGProcessingTask else { return }
            self?.handle(task: processingTask, type: .processing)
        }
    }
    
    func scheduleBackgroundTasks() {
        scheduleAppRefreshTask()
        scheduleProcessingTask()
    }
}

private extension BackgroundTasksService {
    
    enum TaskType: String {
        case appRefresh = "AppRefresh"
        case processing = "Processing"
    }
    
    func handle(task: BGTask, type: TaskType) {
        
        switch type {
        case .appRefresh: scheduleAppRefreshTask()
        case .processing: scheduleProcessingTask()
        }
        
        log("[BackgroundTasksService] Handling \(type.rawValue) task")
        
        syncingService
            .sync()
            .sink(receiveCompletion: { [weak self] _ in
                self?.log("[BackgroundTasksService] Handling \(type.rawValue) task - is about to complete")
                task.setTaskCompleted(success: true)
            }, receiveValue: { _ in })
            .store(in: &subscriptionBag)
        
        task.expirationHandler = {
            self.log("[BackgroundTasksService] Handling \(type.rawValue) task - expiration handler")
            task.setTaskCompleted(success: false)
        }
    }
    
    private func scheduleAppRefreshTask() {
        let request = BGAppRefreshTaskRequest(identifier: appRefreshBackgroundTaskIdentifier)
        request.earliestBeginDate = Date(timeIntervalSinceNow: backgroundFetchMinimumTimeInterval)
        
        log("[BackgroundTasksService] Submitting AppRefresh task")
        submit(request: request)
    }
    
    private func scheduleProcessingTask() {
        let request = BGProcessingTaskRequest(identifier: processingBackgroundTaskIdentifier)
        request.requiresNetworkConnectivity = true
        
        log("[BackgroundTasksService] Submitting Processing task")
        submit(request: request)
    }
    
    private func submit(request: BGTaskRequest) {
        DispatchQueue.global().async { [weak self] in
            do {
                try self?.taskScheduler.submit(request)
                self?.log("[BackgroundTasksService] Request submitted")
            } catch {
                self?.log("[BackgroundTasksService] Could not schedule request because of: \(error)")
            }
        }
    }
}
