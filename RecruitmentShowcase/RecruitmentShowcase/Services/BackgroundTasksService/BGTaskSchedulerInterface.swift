//
//  BGTaskSchedulerInterface.swift
//

import Foundation
import BackgroundTasks

protocol BGTaskSchedulerInterface {
    @discardableResult
    func register(forTaskWithIdentifier identifier: String, using queue: DispatchQueue?, launchHandler: @escaping (BGTask) -> Void) -> Bool
    func submit(_ taskRequest: BGTaskRequest) throws
    func cancel(taskRequestWithIdentifier identifier: String)
    func cancelAllTaskRequests()
    func getPendingTaskRequests(completionHandler: @escaping ([BGTaskRequest]) -> Void)
}

extension BGTaskScheduler: BGTaskSchedulerInterface { }
