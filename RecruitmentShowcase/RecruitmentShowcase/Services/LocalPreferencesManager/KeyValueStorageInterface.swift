//
//  KeyValueStorageInterface.swift
//

import Foundation

public protocol KeyValueStorageInterface: AnyObject {

    func data(forKey defaultName: String) -> Data?
    func set(_ value: Any?, forKey defaultName: String)
}

extension UserDefaults: KeyValueStorageInterface { }
