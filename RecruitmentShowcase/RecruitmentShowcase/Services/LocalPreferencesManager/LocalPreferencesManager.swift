//
//  LocalPreferencesManager.swift
//

import Foundation
import Combine
import SwiftUI

protocol LocalPreferencesManagerInterface {
    var todoSortingPreference: AnyPublisher<TodoSortingPreference, Never> { get }
    func updateTodoSortingPreference(to preference: TodoSortingPreference)
}

final class LocalPreferencesManager: LocalPreferencesManagerInterface {
    
    var todoSortingPreference: AnyPublisher<TodoSortingPreference, Never> {
        todoSortingPreferenceSubject.eraseToAnyPublisher()
    }
    
    private var todoSortingPreferenceSubject = CurrentValueSubject<TodoSortingPreference, Never>(.default)
    
    private let keyValueStorage: KeyValueStorageInterface
    
    init(keyValueStorage: KeyValueStorageInterface) {
        self.keyValueStorage = keyValueStorage
        
        readTodoSortingPreference()
    }
    
    func updateTodoSortingPreference(to preference: TodoSortingPreference) {
        guard let data = try? JSONEncoder().encode(preference) else { return }
        keyValueStorage.set(data, forKey: LocalPreferenceKey.todoSortingPreference.rawValue)
        todoSortingPreferenceSubject.send(preference)
    }
}

private extension LocalPreferencesManager {
    
    func readTodoSortingPreference() {
        guard
            let preferenceData = keyValueStorage.data(forKey: LocalPreferenceKey.todoSortingPreference.rawValue),
            let preference = try? JSONDecoder().decode(TodoSortingPreference.self, from: preferenceData)
        else { return }
        todoSortingPreferenceSubject.send(preference)
    }
}

extension LocalPreferencesManager {
    enum LocalPreferenceKey: String {
        case todoSortingPreference
    }
}
