//
//  TodoSortingPreference.swift
//

import Foundation

struct TodoSortingPreference: Codable, Equatable {
    let order: Order
    let parameter: Parameter
    
    enum Order: Int, Codable, CaseIterable {
        case ascending
        case descending
    }
    
    enum Parameter: String, Codable, CaseIterable {
        case done
        case createdAt
        case updatedAt
    }
    
    static var `default` = TodoSortingPreference(order: .ascending, parameter: .createdAt)
}
