//
//  TodosRequests.swift
//

import Foundation
import Networking

struct GetTodosRequest: HTTPRequest {
    
    typealias ResponseType = GetTodosResponse
    
    let endpointString: String
    let httpMethod: HTTPMethod = .get
    
    init(endpointString: String) {
        self.endpointString = endpointString
    }
}

struct AddTodoRequest: HTTPRequest {
    
    typealias ResponseType = NetworkTodo
    
    let endpointString: String
    let httpMethod: HTTPMethod = .post
    let body: HTTPBody<NetworkTodo>?
    
    init(endpointString: String, body: HTTPBody<NetworkTodo>) {
        self.endpointString = endpointString
        self.body = body
    }
}

struct BulkAddTodosRequest: HTTPRequest {
    
    typealias ResponseType = [NetworkTodo]
    
    let endpointString: String
    let httpMethod: HTTPMethod = .post
    var additionalPathComponents: [String]? = ["bulk"]
    let body: HTTPBody<[NetworkTodo]>?
    
    init(endpointString: String, body: HTTPBody<[NetworkTodo]>) {
        self.endpointString = endpointString
        self.body = body
    }
}

struct UpdateTodoRequest: HTTPRequest {
    
    typealias ResponseType = NetworkTodo
    
    let endpointString: String
    let httpMethod: HTTPMethod = .put
    let body: HTTPBody<NetworkTodo>?
    
    init(endpointString: String, body: HTTPBody<NetworkTodo>) {
        self.endpointString = endpointString
        self.body = body
    }
}

struct BulkUpdateTodosRequest: HTTPRequest {
    
    typealias ResponseType = [NetworkTodo]
    
    let endpointString: String
    let httpMethod: HTTPMethod = .put
    var additionalPathComponents: [String]? = ["bulk"]
    let body: HTTPBody<[NetworkTodo]>?
    
    init(endpointString: String, body: HTTPBody<[NetworkTodo]>) {
        self.endpointString = endpointString
        self.body = body
    }
}

struct DeleteTodoRequest: HTTPRequest {
    
    typealias ResponseType = NoType
    
    let endpointString: String
    let httpMethod: HTTPMethod = .delete
    let additionalPathComponents: [String]?
    
    init(endpointString: String, todoId: String) {
        self.endpointString = endpointString
        self.additionalPathComponents = [todoId]
    }
}

struct BulkDeleteTodosRequest: HTTPRequest {
    
    typealias ResponseType = NoType
    
    let endpointString: String
    let httpMethod: HTTPMethod = .delete
    let additionalPathComponents: [String]? = ["bulk"]
    let body: HTTPBody<[String]>?
    
    init(endpointString: String, body: HTTPBody<[String]>) {
        self.endpointString = endpointString
        self.body = body
    }
}
