//
//  TodosNetworkClient.swift
//

import Foundation
import Combine
import Networking

enum TodosNetworkClientError: Error {
    case underying(error: Error)
}

protocol TodosNetworkClientInterface {
    func getAll() -> AnyPublisher<GetTodosResponse, TodosNetworkClientError>
    func add(todo: NetworkTodo) -> AnyPublisher<NetworkTodo, TodosNetworkClientError>
    func add(todos: [NetworkTodo]) -> AnyPublisher<[NetworkTodo], TodosNetworkClientError>
    func update(todo: NetworkTodo) -> AnyPublisher<NetworkTodo, TodosNetworkClientError>
    func update(todos: [NetworkTodo]) -> AnyPublisher<[NetworkTodo], TodosNetworkClientError>
    func delete(todoId: String) -> AnyPublisher<Never, TodosNetworkClientError>
    func delete(todoIds: [String]) -> AnyPublisher<Never, TodosNetworkClientError>
}

final class TodosNetworkClient: TodosNetworkClientInterface {
    
    private var todosEndpoint: String {
        endpointsProvider.todos
    }
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    private let networkClient: NetworkClientInterface
    private let endpointsProvider: ServerEndpointsProviderInterface
    
    init(networkClient: NetworkClientInterface,
         endpointsProvider: ServerEndpointsProviderInterface) {
        self.networkClient = networkClient
        self.endpointsProvider = endpointsProvider
    }
    
    func getAll() -> AnyPublisher<GetTodosResponse, TodosNetworkClientError> {
        let request = GetTodosRequest(endpointString: todosEndpoint)
        
        return networkClient
            .makeRequest(request)
            .mapError { .underying(error: $0) }
            .eraseToAnyPublisher()
    }
    
    func add(todo: NetworkTodo) -> AnyPublisher<NetworkTodo, TodosNetworkClientError> {
        let request = AddTodoRequest(
            endpointString: todosEndpoint,
            body: HTTPBody(contentType: .json, content: todo)
        )
        
        return networkClient
            .makeRequest(request)
            .mapError { .underying(error: $0) }
            .eraseToAnyPublisher()
    }
    
    func add(todos: [NetworkTodo]) -> AnyPublisher<[NetworkTodo], TodosNetworkClientError> {
        let request = BulkAddTodosRequest(
            endpointString: todosEndpoint,
            body: HTTPBody(contentType: .json, content: todos)
        )
        
        return networkClient
            .makeRequest(request)
            .mapError { .underying(error: $0) }
            .eraseToAnyPublisher()
    }

    func update(todo: NetworkTodo) -> AnyPublisher<NetworkTodo, TodosNetworkClientError> {
        let request = UpdateTodoRequest(
            endpointString: todosEndpoint,
            body: HTTPBody(contentType: .json, content: todo)
        )
        
        return networkClient
            .makeRequest(request)
            .mapError { .underying(error: $0) }
            .eraseToAnyPublisher()
    }
    
    func update(todos: [NetworkTodo]) -> AnyPublisher<[NetworkTodo], TodosNetworkClientError> {
        let request = BulkUpdateTodosRequest(
            endpointString: todosEndpoint,
            body: HTTPBody(contentType: .json, content: todos)
        )
        
        return networkClient
            .makeRequest(request)
            .mapError { .underying(error: $0) }
            .eraseToAnyPublisher()
    }
    
    func delete(todoId: String) -> AnyPublisher<Never, TodosNetworkClientError> {
        let request = DeleteTodoRequest(endpointString: todosEndpoint, todoId: todoId)
    
        return networkClient.makeRequest(request)
            .mapError { .underying(error: $0) }
            .ignoreOutput()
            .eraseToAnyPublisher()
    }
    
    func delete(todoIds: [String]) -> AnyPublisher<Never, TodosNetworkClientError> {
        let request = BulkDeleteTodosRequest(
            endpointString: todosEndpoint,
            body: HTTPBody(contentType: .json, content: todoIds)
        )
    
        return networkClient.makeRequest(request)
            .mapError { .underying(error: $0) }
            .ignoreOutput()
            .eraseToAnyPublisher()
    }
}
