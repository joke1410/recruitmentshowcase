//
//  GetTodosResponse.swift
//

import Foundation

struct GetTodosResponse: Codable {
    let todos: [NetworkTodo]
    let deletedTodoIds: [String]
}
