//
//  NetworkTodo.swift
//

import Foundation

struct NetworkTodo: Codable {
    let id: String
    var content: String
    var moreDetails: String?
    let createdAt: Date
    var updatedAt: Date
    var isDone: Bool
    var priority: Int
}

extension NetworkTodo {
    init(with todo: Todo) {
        self.id = todo.id
        self.content = todo.content
        self.moreDetails = todo.moreDetails
        self.createdAt = todo.createdAt
        self.updatedAt = todo.updatedAt
        self.isDone = todo.isDone
        self.priority = todo.priority.rawValue
    }
    
    var asTodoType: Todo {
        Todo(
            id: id,
            content: content,
            moreDetails: moreDetails,
            createdAt: createdAt,
            updatedAt: updatedAt,
            isDone: isDone,
            priority: .init(rawValue: priority) ?? .default
        )
    }
}
