//
//  TodosService.swift
//

import Foundation
import Combine

protocol TodosServiceInterface {
    var todos: AnyPublisher<[Todo], Never> { get }
    
    func startSyncing()
    
    func fetchTodos() -> AnyPublisher<[Todo], Error>
    func add(todo: Todo) -> AnyPublisher<Todo, Error>
    func update(todo: Todo) -> AnyPublisher<Todo, Error>
    func delete(todo: Todo) -> AnyPublisher<Void, Error>
}

final class TodosService: TodosServiceInterface {
    
    var todos: AnyPublisher<[Todo], Never> {
        todosSubject
            .receive(on: dispatcher.scheduler)
            .eraseToAnyPublisher()
    }
    
    private var todosSubject: CurrentValueSubject<[Todo], Never> = CurrentValueSubject([])
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    private let storage: TodosRealmStorageInterface
    private let syncingService: SyncingServiceInterface
    private let dispatcher: Dispatching
    
    init(storage: TodosRealmStorageInterface,
         syncingService: SyncingServiceInterface,
         dispatcher: Dispatching) {
        self.storage = storage
        self.syncingService = syncingService
        self.dispatcher = dispatcher
        
        dispatcher.dispatch { [unowned self] in
            _ = self.fetchTodos()
        }
        
        subscribeToSyncUpdates()
    }
    
    func startSyncing() {
        syncingService.triggerSyncing()
    }
    
    func fetchTodos() -> AnyPublisher<[Todo], Error>  {
        Future<[Todo], Error>() { [unowned self] promise in
            dispatcher.dispatch { [unowned self] in
                let todos = self.storage.fetchTodos()
                promise(.success(todos))
                todosSubject.send(todos)
            }
        }
        .eraseToAnyPublisher()
    }
    
    func add(todo: Todo) -> AnyPublisher<Todo, Error> {
        Future<Todo, Error>() { [unowned self] promise in
            dispatcher.dispatch { [unowned self] in
                do {
                    try storage.add(todo)
                    promise(.success(todo))
                    self.notifyCurrentTodos()
                    self.syncingService.tryAdd(todo: todo)
                } catch {
                    promise(.failure(error))
                }
            }
        }
        .eraseToAnyPublisher()
    }
    
    func update(todo: Todo) -> AnyPublisher<Todo, Error> {
        Future<Todo, Error>() { [unowned self] promise in
            dispatcher.dispatch { [unowned self] in
                do {
                    try storage.update(todo)
                    promise(.success(todo))
                    self.notifyCurrentTodos()
                    self.syncingService.tryUpdate(todo: todo)
                } catch {
                    promise(.failure(error))
                }
            }
        }
        .eraseToAnyPublisher()
    }
    
    func delete(todo: Todo) -> AnyPublisher<Void, Error> {
        Future<Void, Error>() { [unowned self] promise in
            dispatcher.dispatch { [unowned self] in
                do {
                    try storage.markRemoved(todos: [todo])
                    promise(.success(()))
                    self.notifyCurrentTodos()
                    self.syncingService.tryDelete(todoId: todo.id)
                } catch {
                    promise(.failure(error))
                }
            }
        }
        .eraseToAnyPublisher()
    }
}

private extension TodosService {
    
    func notifyCurrentTodos() {
        dispatcher.dispatch { [unowned self] in
            self.todosSubject.send(storage.fetchTodos())
        }
    }
    
    func subscribeToSyncUpdates() {
        dispatcher.dispatch { [unowned self] in
            self.syncingService
                .synced
                .receive(on: dispatcher.scheduler)
                .sink { [unowned self] _ in
                    self.notifyCurrentTodos()
                }
                .store(in: &subscriptionBag)
        }
    }
}
