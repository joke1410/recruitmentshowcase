//
//  MigrationInterface.swift
//

import Foundation
import RealmSwift

protocol MigrationInterface {
    init(migration: Migration)
    func perform()
}
