//
//  Migration1_2.swift
//

import Foundation
import RealmSwift

final class Migration1_2: MigrationInterface {
    
    let migration: Migration
    
    required init(migration: Migration) {
        self.migration = migration
    }
    
    func perform() {
        migration.enumerateObjects(ofType: TodoRealm.className()) { oldObject, newObject in
            newObject?["priority"] = Int(0)
        }
    }
}
