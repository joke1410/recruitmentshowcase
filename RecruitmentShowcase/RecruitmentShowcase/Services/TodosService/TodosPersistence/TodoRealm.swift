//
//  TodoRealm.swift
//

import Foundation
import RealmSwift

final class TodoRealm: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var content: String = ""
    @objc dynamic var moreDetails: String?
    @objc dynamic var createdAt: Date = Date()
    @objc dynamic var updatedAt: Date = Date()
    @objc dynamic var isDone: Bool = false
    @objc dynamic var isSynced: Bool = false
    @objc dynamic var markedAsRemoved: Bool = false
    @objc dynamic var priority: Int = 0
    
    override static func primaryKey() -> String {
        return "id"
    }
}

extension TodoRealm {
    convenience init(todo: Todo) {
        self.init()
        self.id = todo.id
        self.content = todo.content
        self.moreDetails = todo.moreDetails
        self.createdAt = todo.createdAt
        self.updatedAt = todo.updatedAt
        self.isDone = todo.isDone
        self.priority = todo.priority.rawValue
    }
    
    var asTodoType: Todo {
        Todo(
            id: id,
            content: content,
            moreDetails: moreDetails,
            createdAt: createdAt,
            updatedAt: updatedAt,
            isDone: isDone,
            priority: .init(rawValue: priority) ?? .default
        )
    }
}
