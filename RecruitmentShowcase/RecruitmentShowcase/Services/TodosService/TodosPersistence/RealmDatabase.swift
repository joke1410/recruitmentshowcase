//
//  RealmDatabase.swift
//

import Foundation
import RealmSwift

public protocol RealmDatabaseInterface {
    
    func performIfPossible(with: (_ realm: Realm) -> Void)
    func performIfPossible(with: (_ realm: Realm) throws -> Void) throws
}

final class RealmDatabase: RealmDatabaseInterface {
    
    private let queue: DispatchQueue
    private var configuration: Realm.Configuration
    private var realm: Realm? {
        var _realm: Realm?
        var retry = false
        
        repeat {
            do {
                _realm = try Realm(configuration: configuration)
            } catch {
                if !retry {
                    if let storageURL = configuration.fileURL {
                        _ = try? FileManager.default.removeItem(at: storageURL)
                    }
                }
                retry = !retry
            }
        } while _realm == nil && retry
        
        return _realm
    }
    
    init(configuration: RealmDatabaseConfigurationInterface) {
        self.queue = DispatchQueue(label: configuration.queueName)
        self.configuration = Realm.Configuration(
            schemaVersion: UInt64(configuration.schemaVersion ?? 1),
            deleteRealmIfMigrationNeeded: configuration.deleteRealmIfMigrationNeeded ?? true
        )
        
        var useDefault = false
        if !FileManager.default.fileExists(atPath: configuration.storageURL.path) {
            do {
                try FileManager.default.createDirectory(at: configuration.storageURL, withIntermediateDirectories: true)
            } catch {
                useDefault = false
            }
        }
        
        if !useDefault {
            self.configuration.fileURL = configuration.storageURL.appendingPathComponent(configuration.databaseName)
        }
        
        self.configuration.objectTypes = configuration.storedObjectTypes
        self.configuration.migrationBlock = configuration.migrationBlock
    }
    
    func performIfPossible(with: (Realm) throws -> Void) throws {
        guard let realm = self.realm else { return }
        
        try queue.sync { try with(realm) }
    }
    
    func performIfPossible(with: (Realm) -> Void) {
        guard let realm = self.realm else { return }
        
        queue.sync { with(realm) }
    }
}
