//
//  TodosRealmDatabaseConfiguration.swift
//

import Foundation
import RealmSwift

public protocol RealmDatabaseConfigurationInterface {
    
    var queueName: String { get }
    var databaseName: String { get }
    var storedObjectTypes: [Object.Type] { get }
    var storageURL: URL { get }
    var schemaVersion: Int? { get }
    var deleteRealmIfMigrationNeeded: Bool? { get }
    var migrationBlock: MigrationBlock? { get }
}

struct TodosRealmDatabaseConfiguration: RealmDatabaseConfigurationInterface {
    
    let queueName = "com.pbruz.realm.todos"
    let databaseName = "todos.realm"
    let storedObjectTypes: [Object.Type] = [TodoRealm.self]
    let storageURL: URL
    let schemaVersion: Int? = 2
    let deleteRealmIfMigrationNeeded: Bool? = false
    let migrationBlock: MigrationBlock? = { (migration, oldVersion) in
        switch oldVersion {
        case 1:
            Migration1_2(migration: migration).perform()
        default: break
        }
}

    init(url: URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!) {
        self.storageURL = url
    }
}
