//
//  TodosRealmStorage.swift
//

import Foundation

protocol TodosRealmStorageInterface {
    
    func fetchTodos() -> [Todo]
    func add(_ todo: Todo) throws
    func update(_ todo: Todo) throws
    func mark(isSynced: Bool, todos: [Todo]) throws
    func markRemoved(todos: [Todo]) throws
    func fetchMarkedAsRemovedTodos() -> [Todo]
    func removeTodo(withId id: String) throws
}

final class TodosRealmStorage: TodosRealmStorageInterface {
    
    private let realmContainer: RealmDatabaseInterface
    
    init(realmContainer: RealmDatabaseInterface) {
        self.realmContainer = realmContainer
    }
    
    func fetchTodos() -> [Todo] {
        var todos: [Todo] = []
        realmContainer.performIfPossible { realm in
            let objects = realm.objects(TodoRealm.self).filter("markedAsRemoved == false")
            todos = objects.map { $0.asTodoType }
        }
        return todos
    }
    
    func add(_ todo: Todo) throws {
        try realmContainer.performIfPossible { realm in
            try realm.write {
                realm.add(TodoRealm(todo: todo))
            }
        }
    }
    
    func update(_ todo: Todo) throws {
        try realmContainer.performIfPossible { realm in
            try realm.write {
                guard let object = realm.objects(TodoRealm.self).filter("id = '\(todo.id)'").first else {
                    return
                }
                object.content = todo.content
                object.moreDetails = todo.moreDetails
                object.createdAt = todo.createdAt
                object.updatedAt = todo.updatedAt
                object.isDone = todo.isDone
                object.priority = todo.priority.rawValue
                object.isSynced = false
            }
        }
    }
    
    func mark(isSynced: Bool, todos: [Todo]) throws {
        try realmContainer.performIfPossible { realm in
            try realm.write {
                let objects = realm.objects(TodoRealm.self).filter("id IN %@", todos.map { $0.id })
                objects.forEach {
                    $0.isSynced = isSynced
                }
            }
        }
    }
    
    func markRemoved(todos: [Todo]) throws {
        try realmContainer.performIfPossible { realm in
            try realm.write {
                let objects = realm.objects(TodoRealm.self).filter("id IN %@", todos.map { $0.id })
                objects.forEach {
                    $0.markedAsRemoved = true
                }
            }
        }
    }
    
    func fetchMarkedAsRemovedTodos() -> [Todo] {
        var todos: [Todo] = []
        realmContainer.performIfPossible { realm in
            let objects = realm.objects(TodoRealm.self).filter("markedAsRemoved == true")
            todos = objects.map { $0.asTodoType }
        }
        return todos
    }
    
    func removeTodo(withId id: String) throws {
        try realmContainer.performIfPossible { realm in
            try realm.write {
                guard let object = realm.objects(TodoRealm.self).filter("id = '\(id)'").first else {
                    return
                }
                realm.delete(object)
            }
        }
    }
}
