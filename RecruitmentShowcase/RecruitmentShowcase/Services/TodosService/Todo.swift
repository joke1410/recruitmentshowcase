//
//  Todo.swift
//

import Foundation

struct Todo: Hashable {
    let id: String
    var content: String
    var moreDetails: String?
    let createdAt: Date
    var updatedAt: Date
    var isDone: Bool
    var priority: Priority
    
    init(id: String = UUID().uuidString,
         content: String,
         moreDetails: String?,
         createdAt: Date,
         updatedAt: Date,
         isDone: Bool,
         priority: Priority) {
        self.id = id
        self.content = content
        self.moreDetails = moreDetails
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.isDone = isDone
        self.priority = priority
    }
}

extension Todo {
    enum Priority: Int, CaseIterable {
        case low = 0
        case medium = 1
        case high = 2
        
        static let `default` = Priority.low
    }
}
