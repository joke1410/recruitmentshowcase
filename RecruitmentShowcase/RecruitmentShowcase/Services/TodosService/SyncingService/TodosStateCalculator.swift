//
//  TodosStateCalculator.swift
//

import Foundation

protocol TodosStateCalculatorInterface  {
    func calculateTodosToBeStoredLocally(basedOn state: TodosState) -> [Todo]
    func calculateTodosToBeStoredRemotely(basedOn state: TodosState) -> [Todo]
    func calculateTodosToBeUpdatedLocally(basedOn state: TodosState) -> [Todo]
    func calculateTodosToBeUpdatedRemotely(basedOn state: TodosState) -> [Todo]
    func calculateTodosToBeDeletedLocally(basedOn state: TodosState) -> [Todo]
    func calculateTodosToBeDeletedRemotely(basedOn state: TodosState) -> [Todo]
}

struct TodosState {
    let remoteTodos: [Todo]
    let remoteDeletedTodoIds: [String]
    let localTodos: [Todo]
    let localTodosMarkedAsRemoved: [Todo]
}

struct TodosStateCalculator: TodosStateCalculatorInterface {
    
    func calculateTodosToBeStoredLocally(basedOn state: TodosState) -> [Todo] {
        let allLocalTodos = state.localTodos + state.localTodosMarkedAsRemoved
        let substractedIds = Set(state.remoteTodos.map { $0.id }).subtracting(Set(allLocalTodos.map { $0.id }))
        return state.remoteTodos.filter { substractedIds.contains($0.id) }
    }
    
    func calculateTodosToBeStoredRemotely(basedOn state: TodosState) -> [Todo] {
        let substractedIds = Set(state.localTodos.map { $0.id }).subtracting(Set(state.remoteTodos.map { $0.id } + state.remoteDeletedTodoIds))
        return state.localTodos.filter { substractedIds.contains($0.id) }
    }
    
    func calculateTodosToBeUpdatedLocally(basedOn state: TodosState) -> [Todo] {
        calculateTodosToBeUpdated(basedOn: state).locally
    }
    
    func calculateTodosToBeUpdatedRemotely(basedOn state: TodosState) -> [Todo] {
        calculateTodosToBeUpdated(basedOn: state).remotely
    }
    
    func calculateTodosToBeDeletedLocally(basedOn state: TodosState) -> [Todo] {
        let allLocalTodos = state.localTodos + state.localTodosMarkedAsRemoved
        let idsToRemoveLocally = Set(allLocalTodos.map { $0.id }).intersection(Set(state.remoteDeletedTodoIds))
        return allLocalTodos.filter { idsToRemoveLocally.contains($0.id) }
    }
    
    func calculateTodosToBeDeletedRemotely(basedOn state: TodosState) -> [Todo] {
        state.localTodosMarkedAsRemoved
    }
}

private extension TodosStateCalculator {
    
    func calculateTodosToBeUpdated(basedOn state: TodosState) -> (locally: [Todo], remotely: [Todo]) {
        let intersectedIds = Set(state.localTodos.map { $0.id }).intersection(Set(state.remoteTodos.map { $0.id }))
        var toBeUpdatedLocally: [Todo] = []
        var toBeUpdatedRemotely: [Todo] = []
        
        intersectedIds.forEach { sharedId in
            guard
                let remote = state.remoteTodos.first(where: { $0.id == sharedId }),
                let local = state.localTodos.first(where: { $0.id == sharedId })
            else { return }
            
            let localUpdatedTime = floor(local.updatedAt.timeIntervalSinceReferenceDate)
            let remoteUpdatedTime = floor(remote.updatedAt.timeIntervalSinceReferenceDate)
            
            if localUpdatedTime > remoteUpdatedTime {
                toBeUpdatedRemotely.append(local)
            } else if localUpdatedTime < remoteUpdatedTime {
                toBeUpdatedLocally.append(remote)
            }
        }
        
        return (toBeUpdatedLocally, toBeUpdatedRemotely)
    }
}
