//
//  SyncingService.swift
//

import Foundation
import Combine

protocol SyncingServiceInterface  {
    var synced: AnyPublisher<Date, Never> { get }
    
    func triggerSyncing()
    func sync() -> AnyPublisher<Date, Error>
    func tryAdd(todo: Todo)
    func tryUpdate(todo: Todo)
    func tryDelete(todoId: String)
}

final class SyncingService: SyncingServiceInterface, Logging {
    
    enum SyncingError: Error { case error(underlying: Error) }
    
    var synced: AnyPublisher<Date, Never> {
        syncedSubject.eraseToAnyPublisher()
    }
    private let syncedSubject = PassthroughSubject<Date, Never>()
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    private let networkClient: TodosNetworkClientInterface
    private let storage: TodosRealmStorageInterface
    private let stateCalculator: TodosStateCalculatorInterface
    private let dispatcher: Dispatching
    
    init(networkClient: TodosNetworkClientInterface,
         storage: TodosRealmStorageInterface,
         stateCalculator: TodosStateCalculatorInterface = TodosStateCalculator(),
         dispatcher: Dispatching) {
        self.networkClient = networkClient
        self.storage = storage
        self.stateCalculator = stateCalculator
        self.dispatcher = dispatcher
    }
    
    func triggerSyncing() {
        log("[SyncingService] syncing triggered")
        
        dispatcher.dispatch { [unowned self] in
            self.sync()
                .receive(on: dispatcher.scheduler)
                .sink(receiveCompletion: { _ in }, receiveValue: {_ in })
                .store(in: &subscriptionBag)
        }
    }
    
    func sync() -> AnyPublisher<Date, Error> {
        Future<Date, Error>() { [unowned self] promise in
            self.dispatcher.dispatch { [unowned self] in
                self.networkClient
                    .getAll()
                    .mapError { SyncingError.error(underlying: $0) }
                    .receive(on: dispatcher.scheduler)
                    .map { (todos: $0.todos.map { $0.asTodoType }, deletedTodoIds: $0.deletedTodoIds) }
                    .combineLatest(Just(self.storage.fetchTodos()).setFailureType(to: SyncingError.self))
                    .combineLatest(Just(self.storage.fetchMarkedAsRemovedTodos()).setFailureType(to: SyncingError.self))
                    .map {
                        TodosState(
                            remoteTodos: $0.0.todos,
                            remoteDeletedTodoIds: $0.0.deletedTodoIds,
                            localTodos: $0.1,
                            localTodosMarkedAsRemoved: $1
                        )
                    }
                    .flatMap { [unowned self] in self.handleSync(state: $0).mapError { SyncingError.error(underlying: $0) } }
                    .sink(
                        receiveCompletion: { [weak self] completion in
                            let date = Date()
                            self?.syncedSubject.send(date)
                            switch completion {
                            case .failure(let error):
                                promise(.failure(error))
                            case .finished:
                                promise(.success(date))
                            }
                        },
                        receiveValue: { _ in }
                    )
                    .store(in: &subscriptionBag)
            }
        }.eraseToAnyPublisher()
    }
    
    func tryAdd(todo: Todo) {
        dispatcher.dispatch { [unowned self] in
            self.networkClient
                .add(todo: .init(with: todo))
                .receive(on: dispatcher.scheduler)
                .sink(
                    receiveCompletion: { [unowned self] completion in
                        switch completion {
                        case .finished:
                            try? self.storage.mark(isSynced: true, todos: [todo])
                        case .failure:
                            break // ignore error
                        }
                    },
                    receiveValue: { _ in }
                )
                .store(in: &subscriptionBag)
        }
    }
    
    func tryUpdate(todo: Todo) {
        dispatcher.dispatch { [unowned self] in
            self.networkClient
                .update(todo: .init(with: todo))
                .receive(on: dispatcher.scheduler)
                .sink(
                    receiveCompletion: { [unowned self] completion in
                        switch completion {
                        case .finished:
                            try? self.storage.mark(isSynced: true, todos: [todo])
                        case .failure:
                            break // ignore error
                        }
                    },
                    receiveValue: { _ in }
                )
                .store(in: &subscriptionBag)
        }
    }
    
    func tryDelete(todoId: String) {
        dispatcher.dispatch { [unowned self] in
            self.networkClient
                .delete(todoId: todoId)
                .receive(on: dispatcher.scheduler)
                .sink(
                    receiveCompletion: { [unowned self] completion in
                        switch completion {
                        case .finished:
                            try? self.storage.removeTodo(withId: todoId)
                        case .failure:
                            break // ignore error
                        }
                    },
                    receiveValue: { _ in }
                )
                .store(in: &subscriptionBag)
        }
    }
}

private extension SyncingService  {
    
    func handleSync(state: TodosState) -> AnyPublisher<Void, Error> {
        
        let toBeStoredLocally = stateCalculator.calculateTodosToBeStoredLocally(basedOn: state)
        let toBeStoredRemotely = stateCalculator.calculateTodosToBeStoredRemotely(basedOn: state)
        let toBeUpdatedLocally = stateCalculator.calculateTodosToBeUpdatedLocally(basedOn: state)
        let toBeUpdatedRemotely = stateCalculator.calculateTodosToBeUpdatedRemotely(basedOn: state)
        let toBeDeletedLocally = stateCalculator.calculateTodosToBeDeletedLocally(basedOn: state)
        let toBeDeletedRemotely = stateCalculator.calculateTodosToBeDeletedRemotely(basedOn: state)
        
        return storeLocally(todos: toBeStoredLocally)
            .flatMap { [unowned self] in self.storeRemotely(todos: toBeStoredRemotely) }
            .flatMap { [unowned self] in self.updateLocally(todos: toBeUpdatedLocally) }
            .flatMap { [unowned self] in self.updateRemotely(todos: toBeUpdatedRemotely) }
            .flatMap { [unowned self] in self.deleteLocally(todos: toBeDeletedLocally) }
            .flatMap { [unowned self] in self.deleteRemotely(todos: toBeDeletedRemotely) }
            .eraseToAnyPublisher()
    }
    
    func storeLocally(todos: [Todo]) -> AnyPublisher<Void, Error> {
        todos.forEach {
            try? storage.add($0)
        }
        try? storage.mark(isSynced: true, todos: todos)
        
        return Just(()).setFailureType(to: Error.self).eraseToAnyPublisher()
    }
    
    func storeRemotely(todos: [Todo]) -> AnyPublisher<Void, Error> {
        Future<Void, Error>() { [unowned self] promise in
            
            guard !todos.isEmpty else {
                promise(.success(()))
                return
            }
            
            self.networkClient
                .add(todos: todos.map { NetworkTodo(with: $0) })
                .receive(on: dispatcher.scheduler)
                .sink(
                    receiveCompletion: { completion in
                        switch completion {
                        case .finished: promise(.success(()))
                        case .failure(let error): promise(.failure(error))
                        }
                    },
                    receiveValue: { [unowned self] addedTodos in
                        try? self.storage.mark(isSynced: true, todos: addedTodos.map { $0.asTodoType })
                    }
                )
                .store(in: &subscriptionBag)
        }.eraseToAnyPublisher()
    }
    
    func updateLocally(todos: [Todo]) -> AnyPublisher<Void, Error> {
        todos.forEach {
            try? storage.update($0)
        }
        try? storage.mark(isSynced: true, todos: todos)
        
        return Just(()).setFailureType(to: Error.self).eraseToAnyPublisher()
    }

    func updateRemotely(todos: [Todo]) -> AnyPublisher<Void, Error> {
        Future<Void, Error>() { [unowned self] promise in
            
            guard !todos.isEmpty else {
                promise(.success(()))
                return
            }
            
            self.networkClient
                .update(todos: todos.map { NetworkTodo(with: $0) })
                .receive(on: dispatcher.scheduler)
                .sink(
                    receiveCompletion: { completion in
                        switch completion {
                        case .finished: promise(.success(()))
                        case .failure(let error): promise(.failure(error))
                        }
                    },
                    receiveValue: { [unowned self] updatedTodos in
                        try? self.storage.mark(isSynced: true, todos: updatedTodos.map { $0.asTodoType })
                    }
                )
                .store(in: &subscriptionBag)
        }.eraseToAnyPublisher()
    }
    
    func deleteLocally(todos: [Todo]) -> AnyPublisher<Void, Error> {
        todos.forEach {
            try? storage.removeTodo(withId: $0.id)
        }
        return Just(()).setFailureType(to: Error.self).eraseToAnyPublisher()
    }
    
    func deleteRemotely(todos: [Todo]) -> AnyPublisher<Void, Error> {
        Future<Void, Error>() { [unowned self] promise in
            
            guard !todos.isEmpty else {
                promise(.success(()))
                return
            }
            
            let todoIds = todos.map { $0.id }
            
 
            self.networkClient
                .delete(todoIds: todoIds)
                .receive(on: dispatcher.scheduler)
                .sink(
                    receiveCompletion: { [unowned self] completion in
                        switch completion {
                        case .finished:
                            todoIds.forEach {
                                try? self.storage.removeTodo(withId: $0)
                            }
                            promise(.success(()))
                        case .failure(let error):
                            promise(.failure(error))
                        }
                    },
                    receiveValue: { _ in }
                )
                .store(in: &subscriptionBag)
        }.eraseToAnyPublisher()
    }
}
