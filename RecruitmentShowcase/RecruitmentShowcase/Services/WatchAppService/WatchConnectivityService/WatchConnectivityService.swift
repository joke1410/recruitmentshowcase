//
//  WatchConnectivityService.swift
//

import Foundation
import Combine
import WatchCommunicationProtocol

protocol WatchConnectivityServiceInterface {
    func start()
    func send(appState: AppState)
}

final class WatchConnectivityService: WatchConnectivityServiceInterface, Logging {
    
    private var lastKnownAppState: AppState?
    private var subscriptionBag = Set<AnyCancellable>()
    
    private var isSendingDataAllowed: Bool {
        session.isActive && session.isPaired && session.isWatchAppInstalled
    }
    
    private let session: WCSessionWrapperInterface
    private let intentsHandler: WatchIntentsHandlerInterface
    
    init(session: WCSessionWrapperInterface, intentsHandler: WatchIntentsHandlerInterface) {
        self.session = session
        self.intentsHandler = intentsHandler
        
        subscribeToSessionUpdates()
    }
    
    func start() {
        guard session.isSupported else { return }
        
        session.activate()
    }
    
    func send(appState: AppState) {
        lastKnownAppState = appState
        send(state: appState)
    }
}

private extension WatchConnectivityService {
    
    func send(state: AppState) {
        
        guard isSendingDataAllowed else { return }
        
        do {
            let stateData = try JSONEncoder().encode(state)
            
            try session.updateApplicationContext(
                [CommunicationProtocolMainObjectKey.appState.rawValue: stateData]
            )
        } catch {
            log("[WatchConnectivity] Send state - error: \(error)")
        }
    }
    
    func handleAsIntent(message: [String : Any]) {
        guard
            let intentData = message[CommunicationProtocolMainObjectKey.intent.rawValue] as? Data,
            let intentTypeString = message[CommunicationProtocolMainObjectKey.intentType.rawValue] as? String,
            let intentType = IntentType(rawValue: intentTypeString)
        else {
            log("[WatchConnectivity] Handle intent data - not a valid intent")
            return
        }
        
        handle(intentData: intentData, intentType: intentType)
    }
    
    func handle(intentData: Data, intentType: IntentType) {
        
        do {
            switch intentType {
            case .toggleDone:
                let intent = try ToggleDoneIntent.decode(from: intentData)
                intentsHandler.handle(intent: intent)
            case .delete:
                let intent = try DeleteIntent.decode(from: intentData)
                intentsHandler.handle(intent: intent)
            }
        } catch {
            log("[WatchConnectivity] Handle intent data - error: \(error)")
        }
    }
    
    func subscribeToSessionUpdates() {
        session
            .sessionDidDeactivate
            .sink { [unowned self] in
                self.session.activate()
            }
            .store(in: &subscriptionBag)
            
        session
            .sessionActivationCompleted
            .sink { [unowned self] in
                if let appState = lastKnownAppState {
                    self.send(state: appState)
                }
            }.store(in: &subscriptionBag)
        
        session
            .sessionDidReceiveMessage
            .sink { [unowned self] message in
                self.handleAsIntent(message: message)
            }.store(in: &subscriptionBag)
    }
}

private extension Intent {
    static func decode(from data: Data) throws -> Self {
        try JSONDecoder().decode(Self.self, from: data)
    }
}
