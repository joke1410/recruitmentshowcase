//
//  WatchSessionWrapper.swift
//

import Foundation
import WatchConnectivity
import Combine

final class WCSessionWrapper: NSObject & WCSessionWrapperInterface {
    
    var isActive: Bool {
        session.activationState == .activated
    }
    
    var isPaired: Bool {
        session.isPaired
    }
    
    var isWatchAppInstalled: Bool {
        session.isWatchAppInstalled
    }
    
    var isSupported: Bool {
        session.isSupported
    }
    
    var sessionDidDeactivate: AnyPublisher<Void, Never> {
        sessionDidDeactivateSubject.eraseToAnyPublisher()
    }
    
    var sessionActivationCompleted: AnyPublisher<Void, Never> {
        sessionActivationCompletedSubject.eraseToAnyPublisher()
    }
    
    var sessionDidReceiveMessage: AnyPublisher<[String: Any], Never> {
        sessionDidReceiveMessageSubject.eraseToAnyPublisher()
    }
    
    private var sessionDidDeactivateSubject = PassthroughSubject<Void, Never>()
    private var sessionActivationCompletedSubject = PassthroughSubject<Void, Never>()
    private var sessionDidReceiveMessageSubject = PassthroughSubject<[String: Any], Never>()

    private let session: WCSessionInterface
    
    init(session: WCSessionInterface) {
        self.session = session
        super.init()
        self.session.delegate = self
    }
    
    func activate() {
        session.activate()
    }
    
    func updateApplicationContext(_ applicationContext: [String: Any]) throws {
        try session.updateApplicationContext(applicationContext)
    }
}
protocol WCSessionWrapperInterface {
    
    var isActive: Bool { get }
    var isPaired: Bool { get }
    var isWatchAppInstalled: Bool { get }
    var isSupported: Bool { get }
    
    var sessionDidDeactivate: AnyPublisher<Void, Never> { get }
    var sessionActivationCompleted: AnyPublisher<Void, Never> { get }
    var sessionDidReceiveMessage: AnyPublisher<[String: Any], Never> { get }
    
    func activate()
    func updateApplicationContext(_ applicationContext: [String : Any]) throws
}

extension WCSessionWrapper: WCSessionDelegate {
    
    func sessionDidBecomeInactive(_ session: WCSession) { /* no-op */ }
    
    func sessionDidDeactivate(_ session: WCSession) {
        sessionDidDeactivateSubject.send()
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        sessionActivationCompletedSubject.send()
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String: Any]) {
        sessionDidReceiveMessageSubject.send(message)
    }
}

protocol WCSessionInterface: AnyObject {
    
    var delegate: WCSessionDelegate? { get set }
    var isPaired: Bool { get }
    var isWatchAppInstalled: Bool { get }
    var isSupported: Bool { get }
    
    var activationState: WCSessionActivationState { get }
    
    func activate()
    func updateApplicationContext(_ applicationContext: [String: Any]) throws
}

extension WCSession: WCSessionInterface {
    
    var isSupported: Bool {
        WCSession.isSupported()
    }
}
