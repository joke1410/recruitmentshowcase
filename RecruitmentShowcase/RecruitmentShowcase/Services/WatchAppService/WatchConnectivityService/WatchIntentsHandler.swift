//
//  WatchIntentsHandler.swift
//

import Foundation
import WatchCommunicationProtocol
import Combine

protocol WatchIntentsHandlerInterface {
    func handle(intent: ToggleDoneIntent)
    func handle(intent: DeleteIntent)
}

final class WatchIntentsHandler: WatchIntentsHandlerInterface {
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    private let todosService: TodosServiceInterface
    private let dispatcher: Dispatching
    
    init(todosService: TodosServiceInterface, dispatcher: Dispatching) {
        self.todosService = todosService
        self.dispatcher = dispatcher
    }
    
    func handle(intent: ToggleDoneIntent) {
        fetchTodos()
            .compactMap { $0.first(where: { $0.id == intent.todoId })}
            .flatMap { [unowned self] todo -> AnyPublisher<Todo, Error> in
                var updatedTodo = todo
                updatedTodo.isDone = intent.isDone
                updatedTodo.updatedAt = Date()
                return self.todosService.update(todo: updatedTodo)
            }
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { _ in }
            )
            .store(in: &subscriptionBag)
    }
    
    func handle(intent: DeleteIntent) {
        fetchTodos()
            .compactMap { $0.first(where: { $0.id == intent.todoId })}
            .flatMap { [unowned self] in
                self.todosService.delete(todo: $0)
            }
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { _ in }
            )
            .store(in: &subscriptionBag)
    }
}

private extension WatchIntentsHandler {
    
    func fetchTodos() -> AnyPublisher<[Todo], Never> {
        todosService
            .fetchTodos()
            .receive(on: dispatcher.scheduler)
            .replaceError(with: [])
            .eraseToAnyPublisher()
    }
}
