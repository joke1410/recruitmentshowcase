//
//  WatchAppService.swift
//

import Foundation
import Combine
import RealmSwift
import WatchCommunicationProtocol
import CombineSchedulers

protocol WatchAppServiceInterface {
    func start()
}

final class WatchAppService: WatchAppServiceInterface {
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    private let connectivityService: WatchConnectivityServiceInterface
    private let todosService: TodosServiceInterface
    private let dispatcher: Dispatching
    private let asyncScheduler: AnySchedulerOf<DispatchQueue>
    
    init(connectivityService: WatchConnectivityServiceInterface,
         todosService: TodosServiceInterface,
         dispatcher: Dispatching,
         asyncScheduler: AnySchedulerOf<DispatchQueue>
    ) {
        self.connectivityService = connectivityService
        self.todosService = todosService
        self.dispatcher = dispatcher
        self.asyncScheduler = asyncScheduler
    }
    
    func start() {
        dispatcher.dispatch { [unowned self] in
            self.subscribeToTodosUpdates()
            self.connectivityService.start()
        }
    }
}

private extension WatchAppService {
    
    func subscribeToTodosUpdates() {
        todosService
            .todos
            .debounce(for: 0.5, scheduler: asyncScheduler)
            .receive(on: dispatcher.scheduler)
            .map { todos in AppState(todos: todos.map { .from(todo: $0) }) }
            .sink { [unowned self] state in
                self.connectivityService.send(appState: state)
            }
            .store(in: &subscriptionBag)
    }
}

private extension WatchTodo {
    static func from(todo: Todo) -> Self {
        WatchTodo(id: todo.id, title: todo.content, details: todo.moreDetails, isDone: todo.isDone)
    }
}
