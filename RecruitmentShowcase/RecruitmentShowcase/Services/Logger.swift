//
//  Logger.swift
//

import Foundation

final class Logger {

    func log(_ message: String) {
        print("\(Date()): \(message)")
    }
    
    func devLog(_ message: String) {
        print("\(Date()): [DEV] \(message)")
    }
}

protocol Logging { }

extension Logging {
    
    func log(_ message: String) {
        guard let logger = DI.resolver.resolve(Logger.self) else { return }
        logger.log(message)
    }
    
    func devlog(_ message: String) {
        guard let logger = DI.resolver.resolve(Logger.self) else { return }
        logger.devLog(message)
    }
}

