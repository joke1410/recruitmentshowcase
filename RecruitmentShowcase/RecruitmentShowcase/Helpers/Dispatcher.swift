//
//  Dispatcher.swift
//

import Foundation
import Combine
import CombineSchedulers

protocol Dispatching {
    var scheduler: AnySchedulerOf<DispatchQueue> { get }
    func dispatch(_ work: @escaping () -> Void)
}

class Dispatcher {
    let queue: DispatchQueue
    
    init(queue: DispatchQueue) {
        self.queue = queue
    }
}

final class SyncDispatcher: Dispatcher { }

extension SyncDispatcher: Dispatching {
    
    var scheduler: AnySchedulerOf<DispatchQueue> {
        DispatchQueue.immediate.eraseToAnyScheduler()
    }
    
    func dispatch(_ work: @escaping () -> Void) {
        queue.sync(execute: work)
    }
}

final class AsyncDispatcher: Dispatcher { }
 
extension AsyncDispatcher: Dispatching {
    
    var scheduler: AnySchedulerOf<DispatchQueue> {
        queue.eraseToAnyScheduler()
    }
    
    func dispatch(_ work: @escaping () -> Void) {
        queue.async(execute: work)
    }
}
