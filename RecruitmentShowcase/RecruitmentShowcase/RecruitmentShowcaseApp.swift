//
//  RecruitmentShowcaseApp.swift
//

import SwiftUI
import Combine

@main
struct RecruitmentShowcaseApp: App {
    
    @Environment(\.scenePhase) var scenePhase
    
    private var watchAppService: WatchAppServiceInterface?
    private var backgroundTasksService: BackgroundTasksServiceInterface?
    
    init() {
        DI.build()
        
        startServices()
    }
    
    var body: some Scene {
        WindowGroup {
            baseView
        }
        .onChange(of: scenePhase) { newValue in
            if newValue == .background {
                backgroundTasksService?.scheduleBackgroundTasks()
            }
        }
    }
}

private extension RecruitmentShowcaseApp {
    
    var baseView: some View {
        #if InUnitTests
        EmptyView()
        #else
        ContentView()
        #endif
    }
    
    mutating func startServices() {
        #if InUnitTests
        // Services should be started only for actual implementation.
        // Do not start services for unit tests.
        #else
        watchAppService = DI.resolver.resolve(WatchAppServiceInterface.self)!
        watchAppService?.start()
        
        backgroundTasksService = DI.resolver.resolve(BackgroundTasksServiceInterface.self)!
        backgroundTasksService?.registerBackgroundTasks()
        #endif
    }
}
