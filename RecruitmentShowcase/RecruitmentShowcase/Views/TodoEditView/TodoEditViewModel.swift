//
//  TodoEditViewModel.swift
//

import Foundation
import Combine

final class TodoEditViewModel: ObservableObject {
    
    enum ViewState {
        case editing
        case saved
    }
    
    let screenTitle: String
    let saveButtonName: String
    @Published var content: String = ""
    @Published var moreDetails: String = ""
    @Published var priority: Todo.Priority = .default
    @Published var viewState: ViewState = .editing
    
    private var todo: Todo?
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    private let todosService: TodosServiceInterface
    
    init(todo: Todo?, todosService: TodosServiceInterface = DI.resolver.resolve(TodosServiceInterface.self)!) {
        self.todo = todo
        self.todosService = todosService
        
        screenTitle = todo == nil ? "Create" : "Edit"
        saveButtonName = todo == nil ? "Create" : "Save"
        content = todo?.content ?? ""
        moreDetails = todo?.moreDetails ?? ""
        priority = todo?.priority ?? .default
    }
    
    func save() {
        if todo == nil {
            create()
        } else {
            update()
        }
    }
}

private extension TodoEditViewModel {
    
    func create() {
        let date = Date()
        let newTodo = Todo(
            content: content,
            moreDetails: moreDetails.isEmpty ? nil : moreDetails,
            createdAt: date,
            updatedAt: date,
            isDone: false,
            priority: priority
        )
        todosService
            .add(todo: newTodo)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                switch completion {
                case .finished: self?.viewState = .saved
                case .failure: break
                }
            } receiveValue: { _ in }
            .store(in: &subscriptionBag)
    }
    
    func update() {
        guard var updatedTodo = todo else { return }
        updatedTodo.content = content
        updatedTodo.moreDetails = moreDetails.isEmpty ? nil : moreDetails
        updatedTodo.updatedAt = Date()
        updatedTodo.isDone = false
        updatedTodo.priority = priority
        todosService
            .update(todo: updatedTodo)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                switch completion {
                case .finished: self?.viewState = .saved
                case .failure: break
                }
            } receiveValue: { _ in }
            .store(in: &subscriptionBag)
    }
}

