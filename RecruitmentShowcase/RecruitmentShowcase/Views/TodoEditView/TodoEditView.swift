//
//  TodoEditView.swift
//

import SwiftUI

struct TodoEditView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject private var viewModel: TodoEditViewModel
    
    init(todo: Todo? = nil) {
        viewModel = TodoEditViewModel(todo: todo)
        UITextView.appearance().backgroundColor = .clear
    }
    
    var body: some View {
        ZStack {
            Color.App.backgroundGray.ignoresSafeArea()
            VStack {
                topBarSection
                scrollableContent
            }
            .endEditingKeyboardOnTapGesture()
        }
        .onReceive(viewModel.$viewState) { newState in
            if newState == .saved {
                presentationMode.wrappedValue.dismiss()
            }
        }
    }
    
    private var topBarSection: some View {
        HStack {
            Text(viewModel.screenTitle)
                .font(.title)
                .bold()
                .padding(.top)
            Spacer()
            Button(
                action: { presentationMode.wrappedValue.dismiss() },
                label: {
                    Image(systemName: "xmark")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .padding()
                }
            )
        }
        .padding([.leading, .trailing])
    }
    
    private var scrollableContent: some View {
        ScrollView {
            VStack(spacing: 30) {
                VStack(alignment: .leading) {
                    Text("What needs to be done?")
                        .font(.callout)
                        .bold()
                    textEditor(text: $viewModel.content)
                    if viewModel.content.isEmpty {
                        Text("This field is required.")
                            .foregroundColor(Color.App.red)
                            .font(.caption2)
                    }
                }
                VStack(alignment: .leading) {
                    Text("Put some more details if needed:")
                        .font(.callout)
                        .bold()
                    textEditor(text: $viewModel.moreDetails)
                }
                VStack(alignment: .leading) {
                    HStack {
                        Text("Priority:")
                            .font(.callout)
                            .bold()
                        Spacer()
                        PrioritySelector(selected: $viewModel.priority)
                    }
                }
                .frame(maxWidth: .infinity)
                
                Button(viewModel.saveButtonName) {
                    viewModel.save()
                }
                .buttonStyle(PrimaryButtonStyle())
                .disabled(viewModel.content.isEmpty)
            }
            .padding()
        }
    }
    
    private func textEditor(text: Binding<String>) -> some View {
        ZStack {
            Color.white
                .cornerRadius(5)
            TextEditor(text: text)
                .autocapitalization(.none)
                .disableAutocorrection(true)
                .frame(height: 100)
                .padding()
        }
    }
}

struct TodoEditView_Previews: PreviewProvider {
    static var previews: some View {
        TodoEditView()
    }
}
