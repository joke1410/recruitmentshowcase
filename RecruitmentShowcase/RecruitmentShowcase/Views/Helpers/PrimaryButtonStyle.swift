//
//  PrimaryButtonStyle.swift
//

import SwiftUI

struct PrimaryButtonStyle: ButtonStyle {
    @Environment(\.isEnabled) var isEnabled
    
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .frame(height: 32)
            .frame(maxWidth: .infinity)
            .foregroundColor(.white)
            .padding([.top, .bottom], 8)
            .background(isEnabled ? Color.accentColor : .gray)
            .cornerRadius(5)
            .scaleEffect(configuration.isPressed ? 0.98 : 1.0)
            .opacity(configuration.isPressed ? 0.8 : 1)
            .animation(.linear(duration: 0.1), value: configuration.isPressed)
    }
}

struct PrimaryButtonStyle_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            Spacer()
            Button(
                action: { },
                label: { Text("Primary button style")}
            )
            .buttonStyle(PrimaryButtonStyle())
            Spacer()
        }
    }
}
