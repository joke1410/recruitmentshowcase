//
//  ColorExtension.swift
//

import SwiftUI

extension Color {
    enum App {
        static let green = Color(red: 161.0/255, green: 222.0/255, blue: 147.0/255)
        static let yellow = Color(red: 247.0/255, green: 244.0/255, blue: 139.0/255)
        static let red = Color(red: 244.0/255, green: 124.0/255, blue: 124.0/255)
        
        static let backgroundGray = Color(red: 242.0/255, green: 242.0/255, blue: 247.0/255)
    }
}
