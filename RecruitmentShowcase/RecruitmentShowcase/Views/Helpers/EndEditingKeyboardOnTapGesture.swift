//
//  EndEditingKeyboardOnTapGesture.swift
//

import SwiftUI

struct EndEditingKeyboardOnTapGesture: ViewModifier {
    func body(content: Content) -> some View {
        content.simultaneousGesture (
            TapGesture().onEnded {
                UIApplication.shared.endEditing(true)
            }
        )
    }
}

extension View {
    func endEditingKeyboardOnTapGesture() -> some View {
        return modifier(EndEditingKeyboardOnTapGesture())
    }
}

extension UIApplication {
    func endEditing(_ force: Bool) {
        self.windows
            .filter{$0.isKeyWindow}
            .first?
            .endEditing(force)
    }
}
