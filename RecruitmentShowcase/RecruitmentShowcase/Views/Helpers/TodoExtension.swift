//
//  TodoExtension.swift
//

import SwiftUI

extension Todo.Priority {
    
    var name: String {
        switch self {
        case .low: return "Low"
        case .medium: return "Medium"
        case .high: return "High"
        }
    }
    
    var color: Color {
        switch self {
        case .low: return Color.App.green
        case .medium: return Color.App.yellow
        case .high: return Color.App.red
        }
    }
}

extension Todo.Priority: Hashable, Identifiable {
    var id: Self { self }
}
