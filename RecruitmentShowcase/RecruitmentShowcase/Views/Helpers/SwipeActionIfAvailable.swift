//
//  SwipeActionIfAvailable.swift
//

import SwiftUI

enum Edge {
    case leading
    case trailing
}

struct Action {
    let title: String
    let systemImageName: String
    let tintColor: Color?
    let isDestructive: Bool
    let action: () -> Void
}

extension View {
    
    @ViewBuilder
    func swipeActionIfAvailable(edge: Edge, allowsFullSwipe: Bool = true, action: Action) -> some View {
        if #available(iOS 15.0, *) {
            self.swipeActions(
                edge: edge == .leading ? .leading : .trailing,
                allowsFullSwipe: allowsFullSwipe) {
                    Button(role: action.isDestructive ? .destructive : nil) {
                        action.action()
                    } label: {
                        Label(action.title, systemImage: action.systemImageName)
                    }
                    .tint(action.tintColor)
                }
        } else {
            self
        }
    }
}
