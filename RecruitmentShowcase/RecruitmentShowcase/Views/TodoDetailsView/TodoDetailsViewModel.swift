//
//  TodoDetailsViewModel.swift
//

import Foundation
import Combine

final class TodoDetailsViewModel: ObservableObject {
    
    @Published var todo: Todo
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    private let todosService: TodosServiceInterface
    
    init(todo: Todo, todosService: TodosServiceInterface = DI.resolver.resolve(TodosServiceInterface.self)!) {
        self.todo = todo
        self.todosService = todosService
    }
    
    func toggleDone() {
        var todo = todo
        todo.isDone = !todo.isDone
        todo.updatedAt = Date()
        todosService
            .update(todo: todo)
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { [weak self] todo in
                    self?.todo = todo
                }
            ).store(in: &subscriptionBag)
    }
    
    func delete() {
        todosService
            .delete(todo: todo)
            .receive(on: DispatchQueue.main)
            .sink { _ in } receiveValue: { _ in }
            .store(in: &subscriptionBag)
    }
}
