//
//  TodoDetailsView.swift
//

import SwiftUI

struct TodoDetailsView: View {
    
    @Environment(\.presentationMode) private var presentationMode
    
    @State private var showEditView: Bool = false
    
    @ObservedObject private var viewModel: TodoDetailsViewModel
    
    init(todo: Todo) {
        viewModel = TodoDetailsViewModel(todo: todo)
    }
    
    var body: some View {
        ZStack {
            Color.App.backgroundGray.ignoresSafeArea()
            VStack {
                ScrollView {
                    mainInfoSection
                }
                .padding()
                metadataSection
                actionsSection
            }.frame(maxWidth: .infinity)
        }
        .fullScreenCover(isPresented: $showEditView) {
            TodoEditView(todo: viewModel.todo)
        }
        .navigationBarItems(trailing: editButton)
        .navigationBarTitleDisplayMode(.inline)
    }
    
    private var mainInfoSection: some View {
        VStack(alignment: .leading, spacing: 30) {
            titleAndContent(title: "What?", content: viewModel.todo.content)
            if let details = viewModel.todo.moreDetails {
                titleAndContent(title: "Details", content: details)
            }
        }
        .frame(maxWidth: .infinity)
    }
    
    private func titleAndContent(title: String, content: String) -> some View {
        VStack(alignment: .leading) {
            Text(title)
                .bold()
            Text(content)
                .fixedSize(horizontal: false, vertical: true)
                .padding()
                .foregroundColor(Color(red: 50.0/255, green: 50.0/255, blue: 50.0/255))
        }.frame(maxWidth: .infinity, alignment: .leading)
    }
    
    private var metadataSection: some View {
        HStack {
            Text("Updated at:")
                .bold()
            Text(DateFormatter.custom.string(from: viewModel.todo.updatedAt))
        }
        .font(.caption2)
        .foregroundColor(.gray)
    }
    
    private var actionsSection: some View {
        VStack(spacing: 10) {
            Button("Mark as \(viewModel.todo.isDone ? "NOT DONE" : "DONE")") {
                viewModel.toggleDone()
            }
            .buttonStyle(PrimaryButtonStyle())
            Button("Delete") {
                viewModel.delete()
            }
            .buttonStyle(PrimaryButtonStyle())
        }
        .padding([.leading, .trailing, .bottom])
    }
    
    private var editButton: some View {
        Button(
            action: {
                showEditView.toggle()
            }, label: {
                Image(systemName: "square.and.pencil")
            }
        )
    }
}

struct TodoDetailsView_Previews: PreviewProvider {
    
    static var previews: some View {
        TodoDetailsView(todo: Todo(
            id: "id",
            content: "content",
            moreDetails: "details",
            createdAt: Date(),
            updatedAt: Date(),
            isDone: true,
            priority: .low)
        )
    }
}

extension DateFormatter {
    static let custom: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        return formatter
    }()
}
