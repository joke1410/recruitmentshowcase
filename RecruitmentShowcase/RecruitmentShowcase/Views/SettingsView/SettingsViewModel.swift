//
//  SettingsViewModel.swift
//

import Foundation
import Combine

final class SettingsViewModel: ObservableObject {
    
    @Published var sortingParameterIndex = 0
    @Published var sortingOrderIndex = 0
    let sortingParameters = TodoSortingPreference.Parameter.allCases
    let sortingOrderOptions = TodoSortingPreference.Order.allCases
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    private let todosService: TodosServiceInterface
    private let localPreferencesManager: LocalPreferencesManagerInterface
    
    init(todosService: TodosServiceInterface = DI.resolver.resolve(TodosServiceInterface.self)!,
        localPreferenceManager: LocalPreferencesManagerInterface = DI.resolver.resolve(LocalPreferencesManagerInterface.self)!) {
        self.todosService = todosService
        self.localPreferencesManager = localPreferenceManager
        
        subscribeToLocalPreferencesUpdates()
    }
    
    func startSyncing()  {
        todosService.startSyncing()
    }
    
    func updateSortingParameter(to index: Int) {
        sortingParameterIndex = index
        updateSortingPreference()
        
    }
    
    func updateSortingOrder(to index: Int) {
        sortingOrderIndex = index
        updateSortingPreference()
    }
}

private extension SettingsViewModel {
    
    func subscribeToLocalPreferencesUpdates() {
        localPreferencesManager
            .todoSortingPreference
            .receive(on: DispatchQueue.main)
            .sink{ [weak self] sortingPreference in
                self?.handleSortingOption(basedOn: sortingPreference.parameter)
            }.store(in: &subscriptionBag)
    }
    
    func handleSortingOption(basedOn parameter: TodoSortingPreference.Parameter) {
        guard let index = sortingParameters.firstIndex(where: { $0 == parameter }) else { return }
        sortingParameterIndex = index
    }
    
    func updateSortingPreference() {
        let order = sortingOrderOptions[sortingOrderIndex]
        let parameter = sortingParameters[sortingParameterIndex]
        localPreferencesManager.updateTodoSortingPreference(
            to: .init(order: order, parameter: parameter)
        )
    }
}

extension TodoSortingPreference.Parameter {
    var name: String {
        switch self {
        case .done: return "Status"
        case .createdAt: return "Creation date"
        case .updatedAt: return "Updating date"
        }
    }
}

extension TodoSortingPreference.Order {
    var name: String {
        switch self {
        case .ascending: return "Ascending"
        case .descending: return "Descending"
        }
    }
}
