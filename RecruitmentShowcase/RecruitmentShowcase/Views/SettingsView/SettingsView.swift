//
//  SettingsView.swift
//

import SwiftUI

struct SettingsView: View {
    
    @StateObject private var viewModel = SettingsViewModel()
    
    var body: some View {
        NavigationView {
            content
        }
    }
    
    private var content: some View {
        Form {
            syncingSection
            sortingPrreferenceSection
        }
        .navigationBarTitle("Settings")
    }
    
    private var syncingSection: some View {
        Section(header: Text("SYNCING SECTION")) {
            Button("Start syncing now") {
                viewModel.startSyncing()
            }
        }
    }
    
    private var sortingPrreferenceSection: some View {
        Section(header: Text("SORTING PREFERENCE")) {
            Picker(
                selection: Binding<Int>(
                    get: { viewModel.sortingParameterIndex },
                    set: { viewModel.updateSortingParameter(to: $0) }
                ),
                label: Text("Sort by:")) {
                    ForEach(0 ..< viewModel.sortingParameters.count) {
                        Text(viewModel.sortingParameters[$0].name)
                    }
                }
            Picker(
                selection: Binding<Int>(
                    get: { viewModel.sortingOrderIndex },
                    set: { viewModel.updateSortingOrder(to: $0) }
                ),
                label: Text("Order:")) {
                    ForEach(0 ..< viewModel.sortingOrderOptions.count) {
                        Text(viewModel.sortingOrderOptions[$0].name)
                    }
                }
        }
    }
}
