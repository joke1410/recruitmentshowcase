//
//  PrioritySelector.swift
//

import SwiftUI

struct PrioritySelector: View {
    
    @Binding var selected: Todo.Priority
    
    var body: some View {
        HStack {
            ForEach(Todo.Priority.allCases) { priority in
                PrioritySelectorItem(
                    priority: priority,
                    isSelected: Binding(
                        get: { selected == priority },
                        set: { _ in withAnimation(.linear(duration: 0.25)) { selected = priority } }
                    )
                )
            }
        }
    }
}

struct PrioritySelector_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.App.backgroundGray
            PrioritySelector(selected: .constant(.medium))
        }
    }
}
