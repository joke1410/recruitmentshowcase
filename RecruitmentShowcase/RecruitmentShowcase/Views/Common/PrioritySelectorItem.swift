//
//  PrioritySelectorItem.swift
//

import SwiftUI

struct PrioritySelectorItem: View {
    
    let priority: Todo.Priority
    @Binding var isSelected: Bool
    
    var body: some View {
        Text(priority.name)
            .padding(12)
            .background(priority.color.opacity(isSelected ? 1 : 0.5))
            .cornerRadius(5)
            .conditionalModifier(isSelected) { view in
                view.overlay(
                    RoundedRectangle(cornerRadius: 5)
                        .stroke(Color.accentColor, lineWidth: 2)
                )
            }
            .onTapGesture {
                isSelected = true
            }
    }
}
