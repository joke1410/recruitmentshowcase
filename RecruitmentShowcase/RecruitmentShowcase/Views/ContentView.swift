//
//  ContentView.swift
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        
        TabView {
            TodosListView()
                .tabItem {
                    Label("Todos", systemImage: "list.dash")
                }
            SettingsView()
                .tabItem {
                    Label("Settings", systemImage: "gear")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
