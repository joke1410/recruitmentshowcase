//
//  TodoListItem.swift
//

import SwiftUI

struct TodoListItem: View {
    
    private let actionThreshold: CGFloat = 50
    private let commitThreshold: CGFloat = 200
    
    @State private var action: Action? = nil
    @State private var offset: CGFloat? = nil
    
    private let todo: Todo
    private var tapHandler: () -> Void
    private var markAsDoneHandler: () -> Void
    private var deleteHandler: () -> Void
    
    init(todo: Todo,
         tapHandler: @escaping () -> Void,
         markAsDoneHandler: @escaping () -> Void,
         deleteHandler: @escaping () -> Void) {
        self.todo = todo
        self.tapHandler = tapHandler
        self.markAsDoneHandler = markAsDoneHandler
        self.deleteHandler = deleteHandler
    }
    
    var body: some View {
        ZStack {
            actionsView
            todoView
            .offset(x: offset ?? 0, y: 0)
            .gesture(dragGesture)
            .gesture(tapGesture)
        }
        .onDisappear {
            action = nil
            offset = nil
        }
    }
    
    private var actionsView: some View {
        HStack {
            Button {
                resetAction()
                markAsDoneHandler()
            } label: {
                Image(systemName: "checkmark").padding()
            }
            Spacer()
            Button {
                resetAction()
                deleteHandler()
            } label: {
                Image(systemName: "trash.fill").padding()
            }
            .foregroundColor(.red)
        }
        .padding([.leading, .trailing])
    }
    
    private var todoView: some View {
        ZStack {
            Color.white
            Group {
                todo.priority.color.opacity(todo.isDone ? 0.4 : 0.8)
                HStack(spacing: 0) {
                    if todo.isDone {
                        Image(systemName: "checkmark").padding(18)
                    }
                    Text(todo.content)
                        .strikethrough(todo.isDone)
                        .fontWeight(todo.isDone ? .regular : .medium)
                        .padding([.top, .bottom, .trailing], 18)
                        .padding(.leading, todo.isDone ? 0 : 18)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .fixedSize(horizontal: false, vertical: true)
                        .multilineTextAlignment(.leading)
                }
            }
            .conditionalModifier(!todo.isDone) { view in
                view.overlay(
                    RoundedRectangle(cornerRadius: 5)
                        .stroke(todo.priority.color, lineWidth: 3)
                )
            }
            .opacity(todo.isDone ? 0.5 : 1)
        }
        .foregroundColor(todo.isDone ? .gray : Color(white: 0.3))
        .cornerRadius(5)
        .padding([.leading, .trailing])
    }
    
    private var dragGesture: some Gesture {
        DragGesture(minimumDistance: 20)
            .onChanged { handleDragChanged(dragOffset: $0.translation.width) }
            .onEnded { handleDragEnded(dragOffset: $0.translation.width) }
    }
    
    private func handleDragChanged(dragOffset: CGFloat) {
        guard shouldHandleDrag(dragOffset: dragOffset) else { return }
        
        var newOffset = dragOffset
        
        switch action {
        case .markAsDone: newOffset += actionThreshold
        case .delete: newOffset -= actionThreshold
        default: break
        }
       
        offset = newOffset
    }
    
    private func handleDragEnded(dragOffset: CGFloat) {
        guard shouldHandleDrag(dragOffset: dragOffset) else {
            resetAction()
            return
        }
        
        if dragOffset > 0  {
            if dragOffset > commitThreshold {
                resetAction()
                markAsDoneHandler()
            } else if dragOffset > actionThreshold {
                withAnimation {
                    action = .markAsDone
                    offset = actionThreshold
                }
            } else {
                resetAction()
            }
        } else {
            if dragOffset < -commitThreshold {
                resetAction()
                deleteHandler()
            } else if dragOffset < -actionThreshold {
                withAnimation {
                    action = .delete
                    offset = -actionThreshold
                }
            } else {
                resetAction()
            }
        }
    }
    
    private func shouldHandleDrag(dragOffset: CGFloat) -> Bool {
        guard todo.isDone else { return true }
        if dragOffset > 0 && !(action == .delete) {
            return false
        }  else if action == .delete && dragOffset > actionThreshold {
            return false
        }
        return true
    }
    
    private func resetAction() {
        withAnimation {
            action = nil
            offset = nil
        }
    }
    
    private var tapGesture: some Gesture {
        TapGesture()
            .onEnded {
                resetAction()
                tapHandler()
            }
    }
}

private extension TodoListItem {
    
    enum Action {
        case markAsDone
        case delete
    }
}

struct TodoListItem_Previews: PreviewProvider {
    static var previews: some View {
        TodoListItem(
            todo: Todo(
                content: "content",
                moreDetails: nil,
                createdAt: Date(),
                updatedAt: Date(),
                isDone: false,
                priority: .default
            ),
            tapHandler: { },
            markAsDoneHandler: { },
            deleteHandler: { }
        )
    }
}
