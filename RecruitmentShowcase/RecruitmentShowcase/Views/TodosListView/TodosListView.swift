//
//  TodosListView.swift
//

import SwiftUI

struct TodosListView: View {
    
    @StateObject private var viewModel = TodosListViewModel()
    
    @State private var showCreationView: Bool = false
    @State private var todos: [Todo] = []
    
    @Namespace private var namespace
    @State private var subpage: String?
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.App.backgroundGray.ignoresSafeArea()
                Group {
                    if todos.isEmpty {
                        emptyTodoList
                    } else {
                        list
                    }
                }
                .navigationBarItems(leading: reloadButton, trailing: addButton)
                .navigationTitle("To do list")
                .fullScreenCover(isPresented: $showCreationView) {
                    TodoEditView()
                }
            }
        }
        .onReceive(viewModel.$todos) { list in
            if subpage == nil {
                withAnimation { todos = list }
            } else {
                todos = list
            }
        }
    }
    
    private var emptyTodoList: some View {
        VStack(spacing: 20) {
            Text("Looks like you have nothing to do!")
            Button("Add new task") {
                showCreationView.toggle()
            }
            .buttonStyle(PrimaryButtonStyle())
        }
        .padding()
    }
    
    private var list: some View {
        ScrollView {
            LazyVStack(alignment: .leading, spacing: 8) {
                ForEach(todos, id: \.self) { todo in
                    ZStack {
                        NavigationLink(
                            destination: TodoDetailsView(todo: todo),
                            tag: todo.id,
                            selection: $subpage,
                            label: { EmptyView() }
                        )
                        TodoListItem(
                            todo: todo,
                            tapHandler: { subpage = todo.id },
                            markAsDoneHandler: { viewModel.markAsDone(todo: todo) },
                            deleteHandler: { viewModel.delete(todo: todo) }
                        )
                    }
                    .matchedGeometryEffect(id: todo.id, in: namespace)
                }
            }
        }
    }
    
    private var reloadButton: some View {
        Button(
            action: {
                viewModel.startRefreshing()
            }, label: {
                Image(systemName: "arrow.counterclockwise.circle")
            }
        )
    }
    
    private var addButton: some View {
        Button(
            action: {
                showCreationView.toggle()
            }, label: {
                Image(systemName: "plus.square")
            }
        )
    }
}

struct TodosListView_Previews: PreviewProvider {
    static var previews: some View {
        TodosListView()
    }
}
