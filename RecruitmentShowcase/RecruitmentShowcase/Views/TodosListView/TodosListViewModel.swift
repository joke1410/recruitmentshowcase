//
//  TodosListViewModel.swift
//

import Foundation
import Combine

final class TodosListViewModel: ObservableObject {
    
    @Published var todos: [Todo] = []
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    private let todosService: TodosServiceInterface
    private let localPreferenceManager: LocalPreferencesManagerInterface
    
    init(todosService: TodosServiceInterface = DI.resolver.resolve(TodosServiceInterface.self)!,
         localPreferenceManager: LocalPreferencesManagerInterface = DI.resolver.resolve(LocalPreferencesManagerInterface.self)!) {
        self.todosService = todosService
        self.localPreferenceManager  = localPreferenceManager
        subscribeToTodosUpdates()
    }
    
    func startRefreshing() {
        todosService.startSyncing()
    }
    
    func markAsDone(todo: Todo) {
        var todo = todo
        todo.isDone = true
        todo.updatedAt = Date()
        todosService
            .update(todo: todo)
            .receive(on: DispatchQueue.main)
            .sink { _ in } receiveValue: {_  in }
            .store(in: &subscriptionBag)
    }
    
    func delete(todo: Todo) {
        todosService
            .delete(todo: todo)
            .receive(on: DispatchQueue.main)
            .sink { _ in } receiveValue: { _ in }
            .store(in: &subscriptionBag)
    }
    
    private func subscribeToTodosUpdates() {
        
        Publishers.CombineLatest(
            todosService.todos.map { todos in todos.sorted { $0.priority.rawValue < $1.priority.rawValue  } },
            localPreferenceManager.todoSortingPreference
        )
        .map { todos, sortingPreference in
            let sorted = todos.sorted {
                switch sortingPreference.parameter {
                case .done: return $0.isDone && !$1.isDone
                case .createdAt: return $0.createdAt > $1.createdAt
                case .updatedAt: return $0.updatedAt > $1.updatedAt
                }
            }
            if sortingPreference.order == .descending {
                return sorted
            } else {
                return sorted.reversed()
            }
        }
        .receive(on: DispatchQueue.main)
        .sink { _ in } receiveValue: { [weak self] todos in
            self?.todos = todos
        }
        .store(in: &subscriptionBag)
    }
}

extension Todo: Identifiable { }
