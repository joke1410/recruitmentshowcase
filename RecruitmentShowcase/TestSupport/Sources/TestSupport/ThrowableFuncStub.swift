//
//  ThrowableFuncStub.swift
//

import Foundation

public final class ThrowableFuncStub<ArgumentType, ReturnType> {
    
    public enum Returns {
        case value(ReturnType)
        case closure(() throws -> ReturnType)
    }
    
    public private(set) var counter = 0
    
    public private(set) var arguments: [ArgumentType] = []
    
    private var returns: Returns
    
    public init(returns: Returns) {
        self.returns = returns
    }
    
    public func call(_ arguments: ArgumentType) throws -> ReturnType {
        self.arguments.append(arguments)
        counter += 1
        
        switch returns {
        case .value(let returnValue):
            return returnValue
        case .closure(let closure):
            return try closure()
        }
    }
    
    public func setReturn(_ returns: Returns) {
        self.returns = returns
    }
}

public extension ThrowableFuncStub where ReturnType == Void {
    
    convenience init() {
        self.init(returns: .value(()))
    }
}

public extension ThrowableFuncStub where ArgumentType == Void {

    func call() throws -> ReturnType {
        return try call(())
    }
}
