
public final class FuncStub<ArgumentType, ReturnType> {
    
    public private(set) var counter = 0
    
    public private(set) var arguments: [ArgumentType] = []
    
    private var returns: ReturnType
    
    public init(returns: ReturnType) {
        self.returns = returns
    }
    
    public func call(_ arguments: ArgumentType) -> ReturnType {
        self.arguments.append(arguments)
        counter += 1
        return returns
    }
    
    public func setReturn(value: ReturnType) {
        returns = value
    }
}

public extension FuncStub where ReturnType == Void {
    
    convenience init() {
        self.init(returns: ())
    }
}

public extension FuncStub where ArgumentType == Void {

    func call() -> ReturnType {
        return call(())
    }
}
