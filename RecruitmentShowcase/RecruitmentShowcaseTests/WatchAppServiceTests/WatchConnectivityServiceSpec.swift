//
//  WatchConnectivityServiceSpec.swift
//

@testable import RecruitmentShowcase
import Quick
import Nimble
import Foundation
import Combine
import WatchCommunicationProtocol

final class WatchConnectivityServiceSpec: QuickSpec {
    
    override func spec() {
        
        describe("WatchConnectivityServiceSpec") {
            
            var sut: WatchConnectivityService!
            
            var sessionWrapper: WCSessionWrapperMock!
            var watchIntentsHandler: WatchIntentsHandlerMock!
            
            beforeEach {
                sessionWrapper = WCSessionWrapperMock()
                watchIntentsHandler = WatchIntentsHandlerMock()
                sut = WatchConnectivityService(
                    session: sessionWrapper,
                    intentsHandler: watchIntentsHandler
                )
            }
            
            afterEach {
                sut = nil
                watchIntentsHandler = nil
                sessionWrapper = nil
            }
            
            context("starting service") {
                
                context("session not supported") {
                    
                    beforeEach {
                        sessionWrapper.isSupportedStub.setReturn(value: false)
                        sut.start()
                    }
                    
                    it("session does not get activated") {
                        expect(sessionWrapper.activateStub.counter).to(equal(0))
                    }
                }
                
                context("session supported") {
                    
                    beforeEach {
                        sessionWrapper.isSupportedStub.setReturn(value: true)
                        sut.start()
                    }
                    
                    it("session get activated") {
                        expect(sessionWrapper.activateStub.counter).to(equal(1))
                    }
                }
            }
            
            context("serviceStarted") {
                
                beforeEach {
                    sessionWrapper.isSupportedStub.setReturn(value: true)
                    sut.start()
                }
                
                context("sending events") {
                    
                    context("sending not allowed") {
                        
                        context("because of inactive session") {
                            
                            beforeEach {
                                sessionWrapper.isActiveStub.setReturn(value: false)
                                sessionWrapper.isPairedStub.setReturn(value: true)
                                sessionWrapper.isWatchAppInstalledStub.setReturn(value: true)
                                
                                sut.send(appState: AppState(todos: [WatchTodo(id: "1", title: "1", details: "1", isDone: false)]))
                            }
                            
                            it("update on application context does not happen") {
                                expect(sessionWrapper.updateApplicationContextStub.counter).to(equal(0))
                            }
                        }
                        
                        context("because of not paired watch") {
                            
                            beforeEach {
                                sessionWrapper.isActiveStub.setReturn(value: true)
                                sessionWrapper.isPairedStub.setReturn(value: false)
                                sessionWrapper.isWatchAppInstalledStub.setReturn(value: true)
                                
                                sut.send(appState: AppState(todos: [WatchTodo(id: "1", title: "1", details: "1", isDone: false)]))
                            }
                            
                            it("update on application context does not happen") {
                                expect(sessionWrapper.updateApplicationContextStub.counter).to(equal(0))
                            }
                        }
                        
                        context("because the watch app not being installed") {
                            
                            beforeEach {
                                sessionWrapper.isActiveStub.setReturn(value: true)
                                sessionWrapper.isPairedStub.setReturn(value: true)
                                sessionWrapper.isWatchAppInstalledStub.setReturn(value: false)
                                
                                sut.send(appState: AppState(todos: [WatchTodo(id: "1", title: "1", details: "1", isDone: false)]))
                            }
                            
                            it("update on application context does not happen") {
                                expect(sessionWrapper.updateApplicationContextStub.counter).to(equal(0))
                            }
                        }
                    }
                    
                    context("sending allowed") {
                        
                        beforeEach {
                            sessionWrapper.isActiveStub.setReturn(value: true)
                            sessionWrapper.isPairedStub.setReturn(value: true)
                            sessionWrapper.isWatchAppInstalledStub.setReturn(value: true)
                            
                            sut.send(appState: AppState(todos: [WatchTodo(id: "1", title: "1", details: "1", isDone: false)]))
                        }
                        
                        it("update on application context does happen") {
                            expect(sessionWrapper.updateApplicationContextStub.counter).to(equal(1))
                        }
                        
                        context("handling session updates") {
                            
                            context("session did deactivate") {
                                beforeEach {
                                    sessionWrapper.sessionDidDeactivateSubject.send(())
                                }
                                
                                it("activates session again") {
                                    expect(sessionWrapper.activateStub.counter).to(equal(2))
                                }
                            }
                            
                            context("session activation completed") {
                                beforeEach {
                                    sessionWrapper.sessionActivationCompletedSubject.send(())
                                }
                                
                                it("updates application context with last send state if exists") {
                                    expect(sessionWrapper.updateApplicationContextStub.counter).to(equal(2))
                                }
                            }
                            
                            context("session did receive new message") {
                                beforeEach {
                                    sessionWrapper.sessionDidReceiveMessageSubject.send(ToggleDoneIntent(todoId: "1", isDone: true).asDictionaryMessage!)
                                    sessionWrapper.sessionDidReceiveMessageSubject.send(DeleteIntent(todoId: "1").asDictionaryMessage!)
                                }
                                
                                it("intents handler's methods get called") {
                                    expect(watchIntentsHandler.handleIntentToggleDoneStub.counter).to(equal(1))
                                    expect(watchIntentsHandler.handleIntentDeleteStub.counter).to(equal(1))
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

private extension Intent {
    
    var asDictionaryMessage: [String: Any]? {
        guard let intent = try? JSONEncoder().encode(self) else { return nil }
        
        return [
            CommunicationProtocolMainObjectKey.intent.rawValue: intent,
            CommunicationProtocolMainObjectKey.intentType.rawValue: Self.type.rawValue
        ]
    }
}
