//
//  WatchConnectivityServiceMock.swift
//

import Foundation
@testable import RecruitmentShowcase
import TestSupport
import WatchCommunicationProtocol

final class WatchConnectivityServiceMock: WatchConnectivityServiceInterface {
    
    lazy var startStub = FuncStub<Void, Void>()
    func start() {
        startStub.call()
    }
    
    lazy var sendStub = FuncStub<AppState, Void>()
    func send(appState: AppState) {
        sendStub.call(appState)
    }
}
