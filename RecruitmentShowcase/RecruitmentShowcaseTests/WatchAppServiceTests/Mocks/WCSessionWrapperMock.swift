//
//  WCSessionWrapperMock.swift
//

import Foundation
@testable import RecruitmentShowcase
import TestSupport
import Combine

final class WCSessionWrapperMock: WCSessionWrapperInterface {
    
    lazy var isActiveStub = FuncStub<Void, Bool>(returns: false)
    var isActive: Bool {
        isActiveStub.call()
    }
    
    lazy var isPairedStub = FuncStub<Void, Bool>(returns: false)
    var isPaired: Bool {
        isPairedStub.call()
    }
    
    lazy var isWatchAppInstalledStub = FuncStub<Void, Bool>(returns: false)
    var isWatchAppInstalled: Bool {
        isWatchAppInstalledStub.call()
    }
    
    lazy var isSupportedStub = FuncStub<Void, Bool>(returns: false)
    var isSupported: Bool {
        isSupportedStub.call()
    }
    
    let sessionDidDeactivateSubject = PassthroughSubject<Void, Never>()
    var sessionDidDeactivate: AnyPublisher<Void, Never> {
        sessionDidDeactivateSubject.eraseToAnyPublisher()
    }
    
    let sessionActivationCompletedSubject = PassthroughSubject<Void, Never>()
    var sessionActivationCompleted: AnyPublisher<Void, Never> {
        sessionActivationCompletedSubject.eraseToAnyPublisher()
    }
    
    let sessionDidReceiveMessageSubject = PassthroughSubject<[String : Any], Never>()
    var sessionDidReceiveMessage: AnyPublisher<[String : Any], Never> {
        sessionDidReceiveMessageSubject.eraseToAnyPublisher()
    }
    
    lazy var activateStub = FuncStub<Void, Void>()
    func activate() {
        activateStub.call()
    }
    
    lazy var updateApplicationContextStub = ThrowableFuncStub<[String : Any], Void>()
    func updateApplicationContext(_ applicationContext: [String : Any]) throws {
        try updateApplicationContextStub.call(applicationContext)
    }
}
