//
//  WatchIntentsHandlerMock.swift
//

import Foundation
@testable import RecruitmentShowcase
import TestSupport
import WatchCommunicationProtocol

final class WatchIntentsHandlerMock: WatchIntentsHandlerInterface {
    
    lazy var handleIntentToggleDoneStub = FuncStub<ToggleDoneIntent, Void>()
    func handle(intent: ToggleDoneIntent) {
        handleIntentToggleDoneStub.call(intent)
    }
    
    lazy var handleIntentDeleteStub = FuncStub<DeleteIntent, Void>()
    func handle(intent: DeleteIntent) {
        handleIntentDeleteStub.call(intent)
    }
}
