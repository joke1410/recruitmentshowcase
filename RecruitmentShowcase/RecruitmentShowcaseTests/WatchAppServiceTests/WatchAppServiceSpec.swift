//
//  WatchAppServiceSpec.swift
//

@testable import RecruitmentShowcase
import Quick
import Nimble
import Foundation
import Combine
import WatchCommunicationProtocol

final class WatchAppServiceSpec: QuickSpec {
    
    override func spec() {
        
        describe("WatchAppServiceSpec") {
            
            var sut: WatchAppService!
            
            var todosService: TodosServiceMock!
            var watchConnectivityService: WatchConnectivityServiceMock!
            
            beforeEach {
                todosService = TodosServiceMock()
                watchConnectivityService = WatchConnectivityServiceMock()
                sut = WatchAppService(
                    connectivityService: watchConnectivityService,
                    todosService: todosService,
                    dispatcher: SyncDispatcher(queue: DispatchQueue.global(qos: .userInitiated)),
                    asyncScheduler: DispatchQueue.main.eraseToAnyScheduler()
                )
            }
            
            afterEach {
                sut = nil
                todosService = nil
                watchConnectivityService = nil
            }
            
            context("starting service") {
                
                beforeEach {
                    sut.start()
                }
                
                it("connectivity service gets started") {
                    expect(watchConnectivityService.startStub.counter).to(equal(1))
                }
                
                it("last todos state is passed to connectivity service") {
                    
                    expect(watchConnectivityService.sendStub.counter).toEventually(equal(1))
                }
                
                context("service started") {
                    
                    context("todos list gets updated on todo service") {
                        let mockTodo = Todo.with(id: "mock")

                        beforeEach {
                            todosService.todosSubject.send([mockTodo])
                        }
                        
                        it("passes the todo to connectivity service within AppState") {
                            expect(watchConnectivityService.sendStub.arguments.last?.todos.first?.id).toEventually(equal(mockTodo.id))
                        }
                    }
                }
            }
        }
    }
}
