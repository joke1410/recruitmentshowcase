//
//  WatchIntentsHandlerSpec.swift
//

@testable import RecruitmentShowcase
import Quick
import Nimble
import Foundation
import Combine
import WatchCommunicationProtocol

final class WatchIntentsHandlerSpec: QuickSpec {
    
    override func spec() {
        
        describe("WatchIntentsHandlerSpec") {
            
            var sut: WatchIntentsHandler!
            
            var todosService: TodosServiceMock!
            
            beforeEach {
                todosService = TodosServiceMock()
                sut = WatchIntentsHandler(
                    todosService: todosService,
                    dispatcher: SyncDispatcher(queue: DispatchQueue.global(qos: .userInitiated))
                )
            }
            
            afterEach {
                sut = nil
                todosService = nil
            }
            
            context("handling intents") {
                
                let intentTodoId = "1"
                
                context("todos list empty") {
                    
                    beforeEach {
                        todosService.fetchTodosStub.setReturn(
                            value: Future<[Todo], Error>() { promise in promise(.success([])) }.eraseToAnyPublisher()
                        )
                    }
                    
                    context("ToggleDone intent") {
                        
                        beforeEach {
                            sut.handle(intent: ToggleDoneIntent(todoId: intentTodoId, isDone: true))
                        }
                        
                        it("todos are fetched") {
                            expect(todosService.fetchTodosStub.counter).to(equal(1))
                        }
                        
                        it("update method is not called on todos service") {
                            expect(todosService.updateStub.counter).to(equal(0))
                        }
                        
                    }
                    
                    context("Delete intent") {
                        
                        beforeEach {
                            sut.handle(intent: DeleteIntent(todoId: intentTodoId))
                        }
                        
                        it("todos are fetched") {
                            expect(todosService.fetchTodosStub.counter).to(equal(1))
                        }
                        
                        it("delete method is not called on todos service") {
                            expect(todosService.deleteStub.counter).to(equal(0))
                        }
                    }
                    
                }
                
                context("todos list contain todo with id for the intent handling") {
                    
                    beforeEach {
                        todosService.fetchTodosStub.setReturn(
                            value: Future<[Todo], Error>() { promise in promise(.success([Todo.with(id: intentTodoId)])) }.eraseToAnyPublisher()
                        )
                    }
                    
                    context("ToggleDone intent") {
                        
                        beforeEach {
                            sut.handle(intent: ToggleDoneIntent(todoId: intentTodoId, isDone: true))
                        }
                        
                        it("todos are fetched") {
                            expect(todosService.fetchTodosStub.counter).to(equal(1))
                        }
                        
                        it("update method is called on todos service") {
                            expect(todosService.updateStub.counter).to(equal(1))
                        }
                        
                        it("update method is called with updated values of todo") {
                            let todoSentAsParameter = todosService.updateStub.arguments.last
                            
                            expect(todoSentAsParameter?.id).to(equal(intentTodoId))
                            expect(todoSentAsParameter?.isDone).to(beTrue())
                        }
                    }
                    
                    context("Delete intent") {
                        
                        beforeEach {
                            sut.handle(intent: DeleteIntent(todoId: intentTodoId))
                        }
                        
                        it("todos are fetched") {
                            expect(todosService.fetchTodosStub.counter).to(equal(1))
                        }
                        
                        it("delete method is called on todos service") {
                            expect(todosService.deleteStub.counter).to(equal(1))
                        }
                        
                        it("delete method is called with proper todo") {
                            let todoSentAsParameter = todosService.deleteStub.arguments.last
                            expect(todoSentAsParameter?.id).to(equal(intentTodoId))
                        }
                    }
                }
            }
        }
    }
}
