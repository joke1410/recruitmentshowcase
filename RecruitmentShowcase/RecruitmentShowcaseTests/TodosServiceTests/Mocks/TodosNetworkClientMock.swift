//
//  TodosNetworkClientMock.swift
//

import Foundation
@testable import RecruitmentShowcase
import TestSupport
import Combine

final class TodosNetworkClientMock: TodosNetworkClientInterface {
    
    lazy var getAllStub = FuncStub<Void, AnyPublisher<GetTodosResponse, TodosNetworkClientError>>(
        returns: Future<GetTodosResponse, TodosNetworkClientError>() { promise in
            promise(.success(GetTodosResponse(todos: [], deletedTodoIds: [])))
        }.eraseToAnyPublisher()
    )
    func getAll() -> AnyPublisher<GetTodosResponse, TodosNetworkClientError> {
        getAllStub.call()
    }
    
    lazy var addTodoStub = FuncStub<NetworkTodo, AnyPublisher<NetworkTodo, TodosNetworkClientError>>(
        returns: Future<NetworkTodo, TodosNetworkClientError>() { promise in
            promise(.success(NetworkTodo(with: Todo.with(id: "mockId"))))
        }.eraseToAnyPublisher()
    )
    func add(todo: NetworkTodo) -> AnyPublisher<NetworkTodo, TodosNetworkClientError> {
        addTodoStub.call(todo)
    }
    
    lazy var addTodosStub = FuncStub<[NetworkTodo], AnyPublisher<[NetworkTodo], TodosNetworkClientError>>(
        returns: Future<[NetworkTodo], TodosNetworkClientError>() { promise in
            promise(.success([NetworkTodo(with: Todo.with(id: "mockId"))]))
        }.eraseToAnyPublisher()
    )
    func add(todos: [NetworkTodo]) -> AnyPublisher<[NetworkTodo], TodosNetworkClientError> {
        addTodosStub.call(todos)
    }
    
    lazy var updateTodoStub = FuncStub<NetworkTodo, AnyPublisher<NetworkTodo, TodosNetworkClientError>>(
        returns: Future<NetworkTodo, TodosNetworkClientError>() { promise in
            promise(.success(NetworkTodo(with: Todo.with(id: "mockId"))))
        }.eraseToAnyPublisher()
    )
    func update(todo: NetworkTodo) -> AnyPublisher<NetworkTodo, TodosNetworkClientError> {
        updateTodoStub.call(todo)
    }
    
    lazy var updateTodosStub = FuncStub<[NetworkTodo], AnyPublisher<[NetworkTodo], TodosNetworkClientError>>(
        returns: Future<[NetworkTodo], TodosNetworkClientError>() { promise in
            promise(.success([NetworkTodo(with: Todo.with(id: "mockId"))]))
        }.eraseToAnyPublisher()
    )
    func update(todos: [NetworkTodo]) -> AnyPublisher<[NetworkTodo], TodosNetworkClientError> {
        updateTodosStub.call(todos)
    }
    
    lazy var deleteTodoIdStub = FuncStub<String, AnyPublisher<Never, TodosNetworkClientError>>(
        returns: Future<Void, TodosNetworkClientError>() { promise in
            promise(.success(()))
        }.ignoreOutput().eraseToAnyPublisher()
    )
    func delete(todoId: String) -> AnyPublisher<Never, TodosNetworkClientError> {
        deleteTodoIdStub.call(todoId)
    }
    
    lazy var deleteTodoIdsStub = FuncStub<[String], AnyPublisher<Never, TodosNetworkClientError>>(
        returns: Future<Void, TodosNetworkClientError>() { promise in
            promise(.success(()))
        }.ignoreOutput().eraseToAnyPublisher()
    )
    func delete(todoIds: [String]) -> AnyPublisher<Never, TodosNetworkClientError> {
        deleteTodoIdsStub.call(todoIds)
    }
}
