//
//  TodosRealmStorageMock.swift
//

import Foundation
@testable import RecruitmentShowcase
import TestSupport

final class TodosRealmStorageMock: TodosRealmStorageInterface {
    
    lazy var fetchTodosStub = FuncStub<Void, [Todo]>(returns: [])
    func fetchTodos() -> [Todo] {
        fetchTodosStub.call()
    }
    
    lazy var addStub = ThrowableFuncStub<Todo, Void>()
    func add(_ todo: Todo) throws {
        try addStub.call(todo)
    }
    
    lazy var updateStub = ThrowableFuncStub<Todo, Void>()
    func update(_ todo: Todo) throws {
        try updateStub.call(todo)
    }
    
    lazy var markSyncedStub = ThrowableFuncStub<(Bool, [Todo]), Void>()
    func mark(isSynced: Bool, todos: [Todo]) throws {
        try markSyncedStub.call((isSynced, todos))
    }
    
    lazy var markRemovedStub = ThrowableFuncStub<[Todo], Void>()
    func markRemoved(todos: [Todo]) throws {
        try markRemovedStub.call(todos)
    }
    
    lazy var fetchMarkedAsRemovedTodosStub = FuncStub<Void, [Todo]>(returns: [])
    func fetchMarkedAsRemovedTodos() -> [Todo] {
        fetchMarkedAsRemovedTodosStub.call()
    }
    
    lazy var removeTodoStub = ThrowableFuncStub<String, Void>()
    func removeTodo(withId id: String) throws {
        try removeTodoStub.call(id)
    }
}
