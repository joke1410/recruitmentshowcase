//
//  SyncingServiceMock.swift
//

import Foundation
@testable import RecruitmentShowcase
import TestSupport
import Combine

final class SyncingServiceMock: SyncingServiceInterface {
    
    let syncedSubject = PassthroughSubject<Date, Never>()
    var synced: AnyPublisher<Date, Never> {
        return syncedSubject.eraseToAnyPublisher()
    }
    
    lazy var triggerSyncingStub = FuncStub<Void, Void>()
    func triggerSyncing() {
        triggerSyncingStub.call()
    }
    
    lazy var syncStub = FuncStub<Void, AnyPublisher<Date, Error>>(
        returns: Future<Date, Error>() { promise in
            promise(.success(Date()))
        }.eraseToAnyPublisher()
    )
    func sync() -> AnyPublisher<Date, Error> {
        syncStub.call()
    }
    
    lazy var tryAddStub = FuncStub<Todo, Void>()
    func tryAdd(todo: Todo) {
        tryAddStub.call(todo)
    }
    
    lazy var tryUpdateStub = FuncStub<Todo, Void>()
    func tryUpdate(todo: Todo) {
        tryUpdateStub.call(todo)
    }
    
    lazy var tryDeleteStub = FuncStub<String, Void>()
    func tryDelete(todoId: String) {
        tryDeleteStub.call(todoId)
    }
}
