//
//  TodosStateCalculatorMock.swift
//

import Foundation
@testable import RecruitmentShowcase
import TestSupport

final class TodosStateCalculatorMock: TodosStateCalculatorInterface {
    
    lazy var calculateTodosToBeStoredLocallyStub = FuncStub<TodosState, [Todo]>(returns: [])
    func calculateTodosToBeStoredLocally(basedOn state: TodosState) -> [Todo] {
        calculateTodosToBeStoredLocallyStub.call(state)
    }
    
    lazy var calculateTodosToBeStoredRemotelyStub = FuncStub<TodosState, [Todo]>(returns: [])
    func calculateTodosToBeStoredRemotely(basedOn state: TodosState) -> [Todo] {
        calculateTodosToBeStoredRemotelyStub.call((state))
    }
    
    lazy var calculateTodosToBeUpdatedLocallyStub = FuncStub<TodosState, [Todo]>(returns: [])
    func calculateTodosToBeUpdatedLocally(basedOn state: TodosState) -> [Todo] {
        calculateTodosToBeUpdatedLocallyStub.call((state))
    }
    
    lazy var calculateTodosToBeUpdatedRemotelyStub = FuncStub<TodosState, [Todo]>(returns: [])
    func calculateTodosToBeUpdatedRemotely(basedOn state: TodosState) -> [Todo] {
        calculateTodosToBeUpdatedRemotelyStub.call((state))
    }
    
    lazy var calculateTodosToBeDeletedLocallyStub = FuncStub<TodosState, [Todo]>(returns: [])
    func calculateTodosToBeDeletedLocally(basedOn state: TodosState) -> [Todo] {
        calculateTodosToBeDeletedLocallyStub.call((state))
    }
    
    lazy var calculateTodosToBeDeletedRemotelyStub = FuncStub<TodosState, [Todo]>(returns: [])
    func calculateTodosToBeDeletedRemotely(basedOn state: TodosState) -> [Todo] {
        calculateTodosToBeDeletedRemotelyStub.call((state))
    }
}
