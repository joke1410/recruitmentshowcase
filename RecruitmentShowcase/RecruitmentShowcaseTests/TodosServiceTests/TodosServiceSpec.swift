//
//  TodosServiceSpec.swift
//

@testable import RecruitmentShowcase
import Quick
import Nimble
import Foundation
import Combine

final class TodosServiceSpec: QuickSpec {
    
    override func spec() {
        
        describe("TodosServiceSpec") {
            
            var sut: TodosService!
            var storage: TodosRealmStorageMock!
            var syncingService: SyncingServiceMock!
            
            beforeEach {
                storage = TodosRealmStorageMock()
                syncingService = SyncingServiceMock()
                sut = TodosService(
                    storage: storage,
                    syncingService: syncingService,
                    dispatcher: SyncDispatcher(queue: DispatchQueue.global(qos: .userInitiated))
                )
            }
            
            afterEach {
                sut = nil
                syncingService = nil
                storage = nil
            }
            
            context("when initialized") {
                
                it("fetches data from storage") {
                    expect(storage.fetchTodosStub.counter).to(equal(1))
                }
            }
            
            context("calling methods") {
                
                context("start syncing") {
                    
                    it("calls syncing service method") {
                        sut.startSyncing()
                        expect(syncingService.triggerSyncingStub.counter).to(equal(1))
                    }
                }
                
                context("fetch all") {
                    let mockTodo = Todo.with(id: "mokc")
                    
                    var subscriptionBag = Set<AnyCancellable>()
                    
                    beforeEach {
                        storage.fetchTodosStub.setReturn(value: [mockTodo])
                    }
                    
                    it("calls storage method") {
                        _ = sut.fetchTodos()
                        expect(storage.fetchTodosStub.counter).to(equal(2))
                    }
                    
                    it("returns proper response") {
                        var todos: [Todo] = []
                        
                        sut.fetchTodos().replaceError(with: []).sink { todos = $0 }.store(in: &subscriptionBag)
                        
                        expect(todos.first?.id).to(equal(mockTodo.id))
                    }
                    
                    it("notifies new todo list") {
                        var todos: [Todo] = []
                        
                        sut.todos.sink { todos = $0 }.store(in: &subscriptionBag)
                        
                        _ = sut.fetchTodos()
                        
                        expect(todos.first?.id).to(equal(mockTodo.id))
                    }
                }
                
                context("add") {
                    let fakeTodo = Todo.with(id: "fake")
                    let fakeTodo2 = Todo.with(id: "fake2")
                    
                    var subscriptionBag = Set<AnyCancellable>()
                    
                                        
                    beforeEach {
                        storage.fetchTodosStub.setReturn(value: [fakeTodo, fakeTodo2])
                    }
                    
                    it("calls storage method") {
                        sut.add(todo: fakeTodo2)
                            .sink(receiveCompletion: { _ in }, receiveValue: { _ in })
                            .store(in: &subscriptionBag)
                        expect(storage.addStub.counter).to(equal(1))
                    }
                    
                    context("call to storage succeeds") {
                        
                        it("calls proper method on syncing service") {
                            sut.add(todo: fakeTodo2)
                                .sink(receiveCompletion: { _ in }, receiveValue: { _ in })
                                .store(in: &subscriptionBag)
                            expect(syncingService.tryAddStub.counter).to(equal(1))
                        }
                        
                        it("returns proper response") {
                            var todo: Todo?
                            
                            sut.add(todo: fakeTodo2)
                                .sink(receiveCompletion: { _ in }, receiveValue: { todo = $0 })
                                .store(in: &subscriptionBag)
                            
                            expect(todo?.id).to(equal(fakeTodo2.id))
                        }
                        
                        it("notifies new todo list") {
                            var todos: [Todo] = []
                            
                            sut.todos.sink { todos = $0 }.store(in: &subscriptionBag)
                            
                            _ = sut.add(todo: fakeTodo2)
                            
                            expect(todos).to(equal([fakeTodo, fakeTodo2]))
                        }
                    }
                    
                    context("call to storage throws error") {
                        
                        enum MockError: Error { case mock }
                        
                        beforeEach {
                            storage.addStub.setReturn(.closure({
                                throw MockError.mock
                            }))
                        }
                        
                        it("does not call method on syncing service") {
                            sut.add(todo: fakeTodo2)
                                .sink(receiveCompletion: { _ in }, receiveValue: { _ in })
                                .store(in: &subscriptionBag)
                            expect(syncingService.tryAddStub.counter).to(equal(0))
                        }
                        
                        it("completes with error") {
                            var error: Error?
                            
                            sut.add(todo: fakeTodo2)
                                .sink(receiveCompletion: { completion in
                                    switch completion {
                                    case .failure(let e): error = e
                                    case .finished: break
                                    }
                                }, receiveValue: { _ in })
                                .store(in: &subscriptionBag)
                            
                            expect(error).notTo(beNil())
                        }
                        
                        it("does not notify new todo list") {
                            var todos: [Todo] = []
                            
                            sut.todos.sink { todos = $0 }.store(in: &subscriptionBag)
                            
                            _ = sut.add(todo: fakeTodo2)
                            
                            expect(todos).to(beEmpty())
                        }
                    }
                }
                
                context("update") {
                    let fakeTodo = Todo.with(id: "fake")
                    let fakeTodo2 = Todo.with(id: "fake2")
                    
                    var subscriptionBag = Set<AnyCancellable>()
                    
                                        
                    beforeEach {
                        storage.fetchTodosStub.setReturn(value: [fakeTodo, fakeTodo2])
                    }
                    
                    it("calls storage method") {
                        sut.update(todo: fakeTodo2)
                            .sink(receiveCompletion: { _ in }, receiveValue: { _ in })
                            .store(in: &subscriptionBag)
                        expect(storage.updateStub.counter).to(equal(1))
                    }
                    
                    
                    context("call to storage succeeds") {
                        
                        it("calls proper method on syncing service") {
                            sut.update(todo: fakeTodo2)
                                .sink(receiveCompletion: { _ in }, receiveValue: { _ in })
                                .store(in: &subscriptionBag)
                            expect(syncingService.tryUpdateStub.counter).to(equal(1))
                        }
                    
                        it("returns proper response") {
                            var todo: Todo?
                            
                            sut.update(todo: fakeTodo2)
                                .sink(receiveCompletion: { _ in }, receiveValue: { todo = $0 })
                                .store(in: &subscriptionBag)
                            
                            expect(todo?.id).to(equal(fakeTodo2.id))
                        }
                        
                        it("notifies new todo list") {
                            var todos: [Todo] = []
                            
                            sut.todos.sink { todos = $0 }.store(in: &subscriptionBag)
                            
                            _ = sut.update(todo: fakeTodo2)
                            
                            expect(todos).to(equal([fakeTodo, fakeTodo2]))
                        }
                    }
                    
                    context("call to storage throws error") {
                        
                        enum MockError: Error { case mock }
                        
                        beforeEach {
                            storage.updateStub.setReturn(.closure({
                                throw MockError.mock
                            }))
                        }
                        
                        it("does not call method on syncing service") {
                            sut.update(todo: fakeTodo2)
                                .sink(receiveCompletion: { _ in }, receiveValue: { _ in })
                                .store(in: &subscriptionBag)
                            expect(syncingService.tryUpdateStub.counter).to(equal(0))
                        }
                        
                        it("completes with error") {
                            var error: Error?
                            
                            sut.update(todo: fakeTodo2)
                                .sink(receiveCompletion: { completion in
                                    switch completion {
                                    case .failure(let e): error = e
                                    case .finished: break
                                    }
                                }, receiveValue: { _ in })
                                .store(in: &subscriptionBag)
                            
                            expect(error).notTo(beNil())
                        }
                        
                        it("does not notify new todo list") {
                            var todos: [Todo] = []
                            
                            sut.todos.sink { todos = $0 }.store(in: &subscriptionBag)
                            
                            _ = sut.update(todo: fakeTodo2)
                            
                            expect(todos).to(beEmpty())
                        }
                    }
                }
                
                context("delete") {
                    let fakeTodo = Todo.with(id: "fake")
                    let fakeTodo2 = Todo.with(id: "fake2")
                    
                    var subscriptionBag = Set<AnyCancellable>()
                    
                    beforeEach {
                        storage.fetchTodosStub.setReturn(value: [fakeTodo])
                    }
                    
                    it("calls storage method") {
                        sut.delete(todo: fakeTodo2)
                            .sink(receiveCompletion: { _ in }, receiveValue: { _ in })
                            .store(in: &subscriptionBag)
                        expect(storage.markRemovedStub.counter).to(equal(1))
                    }
                    
                    context("call to storage succeeds") {
                        
                        it("calls proper method on syncing service") {
                            sut.delete(todo: fakeTodo2)
                                .sink(receiveCompletion: { _ in }, receiveValue: { _ in })
                                .store(in: &subscriptionBag)
                            expect(syncingService.tryDeleteStub.counter).to(equal(1))
                        }
                        
                        it("returns positive response") {
                            var finished = false
                            
                            sut.update(todo: fakeTodo2)
                                .sink(receiveCompletion: { completion in
                                    switch completion {
                                    case .finished: finished = true
                                    case .failure: break
                                    }
                                }, receiveValue: { _ in })
                                .store(in: &subscriptionBag)
                            
                            expect(finished).to(beTrue())
                        }
                    
                        it("notifies new todo list") {
                            var todos: [Todo] = []
                            
                            sut.todos.sink { todos = $0 }.store(in: &subscriptionBag)
                            
                            _ = sut.delete(todo: fakeTodo2)
                            
                            expect(todos).to(equal([fakeTodo]))
                        }
                    }
                    
                    context("call to storage throws error") {
                        
                        enum MockError: Error { case mock }
                        
                        beforeEach {
                            storage.markRemovedStub.setReturn(.closure({
                                throw MockError.mock
                            }))
                        }
                        
                        it("does not call method on syncing service") {
                            sut.delete(todo: fakeTodo2)
                                .sink(receiveCompletion: { _ in }, receiveValue: { _ in })
                                .store(in: &subscriptionBag)
                            expect(syncingService.tryDeleteStub.counter).to(equal(0))
                        }
                        
                        it("completes with error") {
                            var error: Error?
                            
                            sut.delete(todo: fakeTodo2)
                                .sink(receiveCompletion: { completion in
                                    switch completion {
                                    case .failure(let e): error = e
                                    case .finished: break
                                    }
                                }, receiveValue: { _ in })
                                .store(in: &subscriptionBag)
                            
                            expect(error).notTo(beNil())
                        }
                        
                        it("does not notify new todo list") {
                            var todos: [Todo] = []
                            
                            sut.todos.sink { todos = $0 }.store(in: &subscriptionBag)
                            
                            _ = sut.delete(todo: fakeTodo2)
                            
                            expect(todos).to(beEmpty())
                        }
                    }
                }
            }
        }
    }
}

extension Todo {
    static func with(id: String, createdAt: Date = Date(), updatedAt: Date = Date(), isDone: Bool = false, priority: Priority = .default) -> Self {
        Todo(id: id, content: "", moreDetails: nil, createdAt: createdAt, updatedAt: updatedAt, isDone: isDone, priority: priority)
    }
}
