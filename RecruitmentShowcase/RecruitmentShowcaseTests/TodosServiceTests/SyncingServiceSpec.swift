//
//  SyncingServiceSpec.swift
//

@testable import RecruitmentShowcase
import Quick
import Nimble
import Foundation
import Combine
import Accessibility

final class SyncingServiceSpec: QuickSpec {
    
    override func spec() {
        
        describe("SyncingServiceSpec") {
            
            var sut: SyncingService!
            var storage: TodosRealmStorageMock!
            var networkClient: TodosNetworkClientMock!
            var stateCalculator: TodosStateCalculatorMock!
            var subscriptionBag: Set<AnyCancellable>!
            
            beforeEach {
                subscriptionBag = Set<AnyCancellable>()
                storage = TodosRealmStorageMock()
                networkClient = TodosNetworkClientMock()
                stateCalculator = TodosStateCalculatorMock()
                sut = SyncingService(
                    networkClient: networkClient,
                    storage: storage,
                    stateCalculator: stateCalculator,
                    dispatcher: SyncDispatcher(queue: DispatchQueue.global(qos: .userInitiated))
                )
            }
            
            afterEach {
                sut = nil
                networkClient = nil
                storage = nil
                stateCalculator = nil
                subscriptionBag.removeAll()
                subscriptionBag = nil
            }
            
            context("methods trying to keep data up to sync") {
               
                context("try add") {
                    
                    it("should call network method") {
                        sut.tryAdd(todo: Todo.with(id: "mock"))
                        expect(networkClient.addTodoStub.counter).to(equal(1))
                    }
                    
                    context("positive network response") {
                        
                        beforeEach {
                            networkClient.addTodoStub.setReturn(
                                value: Just<NetworkTodo>(.with(id: "mockId"))
                                    .setFailureType(to: TodosNetworkClientError.self)
                                    .eraseToAnyPublisher()
                            )
                            
                            sut.tryAdd(todo: Todo.with(id: "mock"))
                        }
                        
                        it("calls storage with marking synced for the todo") {
                            expect(storage.markSyncedStub.counter).to(equal(1))
                            expect(storage.markSyncedStub.arguments.last?.1.first?.id).to(equal("mock"))
                        }
                    }
                    
                    context("network response fails") {
                        
                        beforeEach {
                            networkClient.addTodoStub.setReturn(
                                value: Fail<NetworkTodo, TodosNetworkClientError>(
                                    error: .underying(error: MockError.mock)
                                ).eraseToAnyPublisher()
                            )
                            
                            sut.tryAdd(todo: Todo.with(id: "mock"))
                        }
                        
                        it("does not call storage with marking synced for the todo") {
                            expect(storage.markSyncedStub.counter).to(equal(0))
                        }
                    }
                }
                
                context("try update") {
                    
                    it("should call network method") {
                        sut.tryUpdate(todo: Todo.with(id: "mock"))
                        expect(networkClient.updateTodoStub.counter).to(equal(1))
                    }
                    
                    context("positive network response") {
                        
                        beforeEach {
                            networkClient.updateTodoStub.setReturn(
                                value: Just<NetworkTodo>(.with(id: "mockId"))
                                    .setFailureType(to: TodosNetworkClientError.self)
                                    .eraseToAnyPublisher()
                            )
                            
                            sut.tryUpdate(todo: Todo.with(id: "mock"))
                        }
                        
                        it("calls storage with marking synced for the todo") {
                            expect(storage.markSyncedStub.counter).to(equal(1))
                            expect(storage.markSyncedStub.arguments.last?.1.first?.id).to(equal("mock"))
                        }
                    }
                    
                    context("network response fails") {
                        
                        beforeEach {
                            networkClient.updateTodoStub.setReturn(
                                value: Fail<NetworkTodo, TodosNetworkClientError>(
                                    error: .underying(error: MockError.mock)
                                ).eraseToAnyPublisher()
                            )
                            
                            sut.tryUpdate(todo: Todo.with(id: "mock"))
                        }
                        
                        it("does not call storage with marking synced for the todo") {
                            expect(storage.markSyncedStub.counter).to(equal(0))
                        }
                    }
                }
                
                context("try delete") {
                    
                    it("should call network method") {
                        sut.tryDelete(todoId: "mock")
                        expect(networkClient.deleteTodoIdStub.counter).to(equal(1))
                    }
                    
                    context("positive network response") {
                        
                        beforeEach {
                            networkClient.deleteTodoIdStub.setReturn(
                                value: Future<Void, TodosNetworkClientError>() { promise in
                                    promise(.success(()))
                                }.ignoreOutput().eraseToAnyPublisher()
                            )
                            
                            sut.tryDelete(todoId: "mock")
                        }
                        
                        it("calls storage with removing todo") {
                            expect(storage.removeTodoStub.counter).to(equal(1))
                            expect(storage.removeTodoStub.arguments.last).to(equal("mock"))
                        }
                    }
                    
                    context("network response fails") {
                        
                        beforeEach {
                            networkClient.deleteTodoIdStub.setReturn(
                                value: Fail<Never, TodosNetworkClientError>(
                                    error: .underying(error: MockError.mock)
                                ).eraseToAnyPublisher()
                            )
                            
                            sut.tryDelete(todoId: "mock")
                        }
                        
                        it("does not call storage with removing todo") {
                            expect(storage.removeTodoStub.counter).to(equal(0))
                        }
                    }
                }
            }
            
            context("sync now method") {
                beforeEach {
                    stateCalculator.calculateTodosToBeStoredRemotelyStub.setReturn(value: [.with(id: "sr1"), .with(id: "sr2")])
                    stateCalculator.calculateTodosToBeStoredLocallyStub.setReturn(value: [.with(id: "sl1"), .with(id: "sl2")])
                    stateCalculator.calculateTodosToBeUpdatedRemotelyStub.setReturn(value: [.with(id: "ur1"), .with(id: "ur2")])
                    stateCalculator.calculateTodosToBeUpdatedLocallyStub.setReturn(value: [.with(id: "ul1"), .with(id: "ul2")])
                    stateCalculator.calculateTodosToBeDeletedRemotelyStub.setReturn(value: [.with(id: "dr1"), .with(id: "dr2")])
                    stateCalculator.calculateTodosToBeDeletedLocallyStub.setReturn(value: [.with(id: "dl1"), .with(id: "dl2")])
                    
                    networkClient.addTodosStub.setReturn(
                        value: Just<[NetworkTodo]>([.with(id: "sr1"), .with(id: "sr2")])
                            .setFailureType(to: TodosNetworkClientError.self)
                            .eraseToAnyPublisher()
                    )
                    
                    networkClient.updateTodosStub.setReturn(
                        value: Just<[NetworkTodo]>([.with(id: "ur1"), .with(id: "ur2")])
                            .setFailureType(to: TodosNetworkClientError.self)
                            .eraseToAnyPublisher()
                    )
                    
                    networkClient.deleteTodoIdsStub.setReturn(
                        value: Just<[String]>(["dr1", "dr2"])
                            .setFailureType(to: TodosNetworkClientError.self)
                            .ignoreOutput()
                            .eraseToAnyPublisher()
                    )
                }
                
                context("fetching todos fails") {
                    var isSynced = false
                    
                    beforeEach {
                        networkClient.getAllStub.setReturn(
                            value: Fail<GetTodosResponse, TodosNetworkClientError>(
                                error: .underying(error: MockError.mock)
                            ).eraseToAnyPublisher()
                        )
                        
                        sut.synced.sink { _ in isSynced = true }.store(in: &subscriptionBag)
                    }
                    
                    it("new synced update happens") {
                        sut.triggerSyncing()
                        expect(isSynced).to(beTrue())
                    }
                }
                
                context("happy path") {
                    var isSynced = false

                    beforeEach {
                        sut.triggerSyncing()
                        sut.synced.sink { _ in isSynced = true }.store(in: &subscriptionBag)
                    }

                    it("fetches all todos from network") {
                        expect(networkClient.getAllStub.counter).to(equal(1))
                    }

                    it("fetches all todos from storage") {
                        expect(storage.fetchTodosStub.counter).to(equal(1))
                    }

                    it("fetches todos marked as removed from storage") {
                        expect(storage.fetchMarkedAsRemovedTodosStub.counter).to(equal(1))
                    }

                    it("does the calculations based on gathered data") {
                        expect(stateCalculator.calculateTodosToBeStoredRemotelyStub.counter).to(equal(1))
                        expect(stateCalculator.calculateTodosToBeStoredLocallyStub.counter).to(equal(1))
                        expect(stateCalculator.calculateTodosToBeUpdatedRemotelyStub.counter).to(equal(1))
                        expect(stateCalculator.calculateTodosToBeUpdatedLocallyStub.counter).to(equal(1))
                        expect(stateCalculator.calculateTodosToBeDeletedRemotelyStub.counter).to(equal(1))
                        expect(stateCalculator.calculateTodosToBeDeletedLocallyStub.counter).to(equal(1))
                        
                    }

                    it("calls database methods with proper values") {
                        expect(storage.addStub.counter).to(equal(2))
                        expect(storage.addStub.arguments.map { $0.id }).to(equal(["sl1", "sl2"]))
                        
                        expect(storage.updateStub.counter).to(equal(2))
                        expect(storage.updateStub.arguments.map { $0.id }).to(equal(["ul1", "ul2"]))
                        
                        expect(storage.removeTodoStub.counter).to(equal(4))
                        expect(storage.removeTodoStub.arguments.sorted).to(equal(["dl1", "dl2", "dr1", "dr2"].sorted()))
                    }

                    it("calls network client methods with proper values") {
                        expect(networkClient.addTodosStub.counter).to(equal(1))
                        expect(networkClient.updateTodosStub.counter).to(equal(1))
                        expect(networkClient.deleteTodoIdsStub.counter).to(equal(1))
                    }
                    
                    it("marks the todos as synced in storage") {
                        expect(storage.markSyncedStub.counter).to(equal(4))
                        let values = storage.markSyncedStub.arguments.flatMap { $0.1 }.map { $0.id }.sorted()
                        let expectedValues = ["sr1", "sr2", "sl1", "sl2", "ur1", "ur2", "ul1", "ul2"].sorted()
                        expect(values).to(equal(expectedValues))
                    }
                    
                    it("new synced update happens") {
                        sut.triggerSyncing()
                        expect(isSynced).to(beTrue())
                    }
                }
            }
        }
    }
    
    enum MockError: Error {
        case mock
    }
}

extension NetworkTodo {
    static func with(id: String) -> Self {
        NetworkTodo(with: Todo.with(id: id))
    }
}
