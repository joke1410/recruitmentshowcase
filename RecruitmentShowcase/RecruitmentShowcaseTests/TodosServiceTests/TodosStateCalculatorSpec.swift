//
//  TodosStateCalculatorSpec.swift
//

@testable import RecruitmentShowcase
import Quick
import Nimble
import Foundation
import Combine

final class TodosStateCalculatorSpec: QuickSpec {
    
    override func spec() {
        
        describe("TodosStateCalculatorSpec") {
            
            var sut: TodosStateCalculator!
            
            beforeEach {
                sut = TodosStateCalculator()
            }
            
            afterEach {
                sut = nil
            }
            
            context("doing calculations") {
                
                let state = TodosState(
                    remoteTodos: [
                        Todo.with(id: "s1", updatedAt: Date(timeIntervalSince1970: 1)),
                        Todo.with(id: "s2", updatedAt: Date(timeIntervalSince1970: 2)),
                        Todo.with(id: "r3")
                    ],
                    remoteDeletedTodoIds: ["d1", "d2"],
                    localTodos: [
                        Todo.with(id: "s1", updatedAt: Date(timeIntervalSince1970: 2)),
                        Todo.with(id: "s2", updatedAt: Date(timeIntervalSince1970: 1)),
                        Todo.with(id: "l3")
                    ],
                    localTodosMarkedAsRemoved: [
                        Todo.with(id: "m1"),
                        Todo.with(id: "d1")
                    ]
                )
                
                it("to be stored locally") {
                    let todos = sut.calculateTodosToBeStoredLocally(basedOn: state)
                    let expectedIds = ["r3"]
                    expect(todos.map { $0.id }).to(equal(expectedIds))
                }
                
                it("to be stored remotely") {
                    let todos = sut.calculateTodosToBeStoredRemotely(basedOn: state)
                    let expectedIds = ["l3"]
                    expect(todos.map { $0.id }).to(equal(expectedIds))
                }
                
                it("to be updated locally") {
                    let todos = sut.calculateTodosToBeUpdatedLocally(basedOn: state)
                    let expectedIds = ["s2"]
                    expect(todos.map { $0.id }).to(equal(expectedIds))
                }
                
                it("to be updated remotely") {
                    let todos = sut.calculateTodosToBeUpdatedRemotely(basedOn: state)
                    let expectedIds = ["s1"]
                    expect(todos.map { $0.id }).to(equal(expectedIds))
                }
                
                it("to be deleted locally") {
                    let todos = sut.calculateTodosToBeDeletedLocally(basedOn: state)
                    let expectedIds = ["d1"]
                    expect(todos.map { $0.id }).to(equal(expectedIds))
                }
                
                it("to be deleted remotely") {
                    let todos = sut.calculateTodosToBeDeletedRemotely(basedOn: state)
                    let expectedIds = ["m1", "d1"]
                    expect(todos.map { $0.id }).to(equal(expectedIds))
                }
            }
        }
    }
}
