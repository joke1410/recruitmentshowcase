//
//  KeyValueStorageMock.swift
//

import Foundation
@testable import RecruitmentShowcase
import TestSupport

final class KeyValueStorageMock: KeyValueStorageInterface {
    
    lazy var dataStub = FuncStub<String, Data?>(returns: nil)
    func data(forKey defaultName: String) -> Data? {
        dataStub.call(defaultName)
    }
    
    lazy var setStub = FuncStub<(Any?, String), Void>()
    func set(_ value: Any?, forKey defaultName: String) {
        setStub.call((value, defaultName))
    }
}
