//
//  TodosServiceMock.swift
//

import Foundation
@testable import RecruitmentShowcase
import TestSupport
import Combine

final class TodosServiceMock: TodosServiceInterface {
    
    let todosSubject = CurrentValueSubject<[Todo], Never>([])
    var todos: AnyPublisher<[Todo], Never> {
        todosSubject.eraseToAnyPublisher()
    }
    
    lazy var startSyncingStub = FuncStub<Void, Void>()
    func startSyncing() {
        startSyncingStub.call()
    }
    
    lazy var fetchTodosStub = FuncStub<Void, AnyPublisher<[Todo], Error>>(
        returns: Future<[Todo], Error>() { promise in promise(.success([])) }.eraseToAnyPublisher()
    )
    func fetchTodos() -> AnyPublisher<[Todo], Error> {
        fetchTodosStub.call()
    }
    
    lazy var addStub = FuncStub<Todo, AnyPublisher<Todo, Error>>(
        returns: Future<Todo, Error>() { promise in promise(.success(Todo.with(id: "id"))) }.eraseToAnyPublisher()
    )
    func add(todo: Todo) -> AnyPublisher<Todo, Error> {
        addStub.call(todo)
    }
    
    lazy var updateStub = FuncStub<Todo, AnyPublisher<Todo, Error>>(
        returns: Future<Todo, Error>() { promise in promise(.success(Todo.with(id: "id"))) }.eraseToAnyPublisher()
    )
    func update(todo: Todo) -> AnyPublisher<Todo, Error> {
        updateStub.call(todo)
    }
    
    lazy var deleteStub = FuncStub<Todo, AnyPublisher<Void, Error>>(
        returns: Future<Void, Error>() { promise in promise(.success(())) }.eraseToAnyPublisher()
    )
    func delete(todo: Todo) -> AnyPublisher<Void, Error> {
        deleteStub.call(todo)
    }
}
