//
//  LocalPreferencesManagerSpec.swift
//

@testable import RecruitmentShowcase
import Quick
import Nimble
import Foundation
import Combine

final class LocalPreferencesManagerSpec: QuickSpec {
    
    override func spec() {
        
        describe("LocalPreferencesManagerSpec") {
            
            var sut: LocalPreferencesManager!
            
            var keyValueStorage: KeyValueStorageMock!
            
            beforeEach {
                keyValueStorage = KeyValueStorageMock()
                sut = LocalPreferencesManager(keyValueStorage: keyValueStorage)
            }
            
            afterEach {
                sut = nil
                keyValueStorage = nil
            }
            
            context("after initialisation and no data in storage") {
                var subscriptionBag = Set<AnyCancellable>()
                
                var preference: TodoSortingPreference?
                
                beforeEach {
                    sut.todoSortingPreference.sink {
                        preference = $0
                    }.store(in: &subscriptionBag)
                }
                
                it("todo sorting preference is set to default") {
                    expect(preference).to(equal(TodoSortingPreference.default))
                }
            }
            
            context("updating sorting preference") {
                
                let sortingPreference = TodoSortingPreference(
                    order: .ascending,
                    parameter: .done
                )
                
                var subscriptionBag = Set<AnyCancellable>()
                var preference: TodoSortingPreference?
                
                beforeEach {
                    sut.todoSortingPreference.sink {
                        preference = $0
                    }.store(in: &subscriptionBag)
                    
                    sut.updateTodoSortingPreference(to: sortingPreference)
                }
                
                it("storage set function was called")  {
                    expect(keyValueStorage.setStub.counter).to(equal(1))
                }
                
                it("storage set function was called with proper data with proper key")  {
                    let data = keyValueStorage.setStub.arguments.first?.0
                    let key = keyValueStorage.setStub.arguments.first?.1
                    let preference: TodoSortingPreference? = {
                        guard let data = data as? Data else { return nil }
                        return try? JSONDecoder().decode(TodoSortingPreference.self, from: data)
                    }()
                    
                    expect(preference).to(equal(sortingPreference))
                    expect(key).to(equal(LocalPreferencesManager.LocalPreferenceKey.todoSortingPreference.rawValue))
                }
                
                it("todoSortingPreference publisher gets updated")  {
                    expect(preference).to(equal(sortingPreference))
                }
            }
        }
    }
}
