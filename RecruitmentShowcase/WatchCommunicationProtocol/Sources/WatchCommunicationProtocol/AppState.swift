//
//  AppState.swift
//  

import Foundation

public struct AppState: Codable {
    public var timestamp: Date
    public var todos: [WatchTodo]
    
    public init(timestamp: Date = Date(),
                todos: [WatchTodo]) {
        self.timestamp = timestamp
        self.todos = todos
    }
}

public enum CommunicationProtocolMainObjectKey: String {
    case intent
    case intentType
    case appState
}

public struct WatchTodo: Codable, Hashable {
    public let id: String
    public let title: String
    public let details: String?
    public var isDone: Bool
    
    public init(id: String, title: String, details: String?, isDone: Bool) {
        self.id = id
        self.title = title
        self.details = details
        self.isDone = isDone
    }
}
