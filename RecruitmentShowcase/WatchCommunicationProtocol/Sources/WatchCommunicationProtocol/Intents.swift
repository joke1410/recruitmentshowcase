//
//  Intents.swift
//  

import Foundation

public protocol Intent: Codable {
    static var type: IntentType { get }
    var timestamp: Date { get }
}

public enum IntentType: String {
    case toggleDone
    case delete
}

public extension Intent {
    var type: IntentType {
        return Self.type
    }
}

public struct ToggleDoneIntent: Intent {
    static public let type: IntentType = .toggleDone
    public let timestamp: Date
    public let todoId: String
    public let isDone: Bool
    
    public init(timestamp: Date = Date(),
                todoId: String,
                isDone: Bool) {
        self.timestamp = timestamp
        self.todoId = todoId
        self.isDone = isDone
    }
}

public struct DeleteIntent: Intent {
    static public let type: IntentType = .delete
    public let timestamp: Date
    public let todoId: String
    
    public init(timestamp: Date = Date(),
                todoId: String) {
        self.timestamp = timestamp
        self.todoId = todoId
    }
}
