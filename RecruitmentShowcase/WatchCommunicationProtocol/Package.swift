// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "WatchCommunicationProtocol",
    products: [
        .library(
            name: "WatchCommunicationProtocol",
            targets: ["WatchCommunicationProtocol"]),
    ],
    targets: [
        .target(
            name: "WatchCommunicationProtocol",
            dependencies: [])
    ]
)
