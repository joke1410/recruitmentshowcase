//
//  URLSessionProxyMock.swift
//  

import Foundation
import TestSupport
import Combine
@testable import Networking

final class URLSessionProxyMock: URLSessionProxyInterface  {
    
    lazy var dataTaskPublisherStub = FuncStub<URLRequest, AnyPublisher<(data: Data, response: URLResponse), URLError>>(
        returns: Future<(data: Data, response: URLResponse), URLError>() {
            promise in promise(.success((Data(), URLResponse())))
        }.eraseToAnyPublisher()
    )
    func dataTaskPublisher(for request: URLRequest) -> AnyPublisher<(data: Data, response: URLResponse), URLError> {
        dataTaskPublisherStub.call(request)
    }
}
