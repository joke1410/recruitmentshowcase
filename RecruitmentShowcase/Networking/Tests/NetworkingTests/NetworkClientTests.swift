import XCTest
import TestSupport
import Combine
@testable import Networking

final class NetworkClientTests: XCTestCase {
    
    var sut: NetworkClient!
    var urlSessionProxyMock: URLSessionProxyMock!
    
    private var subscriptionBag = Set<AnyCancellable>()
    
    override func setUp() {
        urlSessionProxyMock = URLSessionProxyMock()
        sut = NetworkClient(sessionProxy: urlSessionProxyMock)
    }
    
    override func tearDown() {
        urlSessionProxyMock = nil
        sut = nil
        subscriptionBag.removeAll()
    }
    
    func testEndpointInvalid() {
        let expectation = expectation(description: "request completed")
        
        var error: NetworkingError?
        
        sut.makeRequest(FakeRequest(endpointString: ""))
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .failure(let e):
                    error = e
                case .finished:
                    break
                }
                expectation.fulfill()
            } receiveValue: { _ in }
            .store(in: &subscriptionBag)
        
        wait(for: [expectation], timeout: 2.0)
        
        switch error {
        case .internal(underlying: let underlying):
            XCTAssert(underlying == .invalidEndpoint)
        default:
            XCTFail()
        }
    }
    
    func testURLSessionProxyCalledButErroredOut() {
        let expectation = expectation(description: "request completed")
        
        urlSessionProxyMock.dataTaskPublisherStub.setReturn(
            value: fakePublisher(
                result: .failure(URLError(.unknown))
            )
        )
        
        var error: NetworkingError?
        
        sut.makeRequest(FakeRequest())
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .failure(let e):
                    error = e
                case .finished:
                    break
                }
                expectation.fulfill()
            } receiveValue: { _ in }
            .store(in: &subscriptionBag)
        
        wait(for: [expectation], timeout: 2.0)
        
        switch error {
        case .other:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testURLSessionProxyCalledWithResponseThatContains404() {
        let expectation = expectation(description: "request completed")
        
        urlSessionProxyMock.dataTaskPublisherStub.setReturn(
            value: fakePublisher(
                result: .success((Data(), HTTPURLResponse.withStatusCode(404)))
            )
        )
        
        var error: NetworkingError?
        
        sut.makeRequest(FakeRequest())
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .failure(let e):
                    error = e
                case .finished:
                    break
                }
                expectation.fulfill()
            } receiveValue: { _ in }
            .store(in: &subscriptionBag)
        
        wait(for: [expectation], timeout: 2.0)
        
        switch error {
        case .network(underlying: let networkError):
            XCTAssertTrue(networkError.code.rawValue == 404)
        default:
            XCTFail()
        }
    }
    
    func testURLSessionProxyCalledWith200ResponseButWrongData() {
        let expectation = expectation(description: "request completed")
        
        urlSessionProxyMock.dataTaskPublisherStub.setReturn(
            value: fakePublisher(
                result: .success((Data(), HTTPURLResponse.withStatusCode(200)))
            )
        )
        
        var error: NetworkingError?
        
        sut.makeRequest(FakeRequest())
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .failure(let e):
                    error = e
                case .finished:
                    break
                }
                expectation.fulfill()
            } receiveValue: { _ in }
            .store(in: &subscriptionBag)
        
        wait(for: [expectation], timeout: 2.0)
        
        switch error {
        case .decoding:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testURLSessionProxyCalledAndDataDecoded() {
        
        let expectation = expectation(description: "dataType returned")
        let fakeText = "hello"
        
        urlSessionProxyMock.dataTaskPublisherStub.setReturn(
            value: fakePublisher(
                result: .success((try! JSONEncoder().encode(DataType(fakeText: fakeText)), HTTPURLResponse.withStatusCode(200)))
            )
        )
        
        var data: DataType? = nil
        
        sut.makeRequest(FakeRequest())
            .receive(on: DispatchQueue.main)
            .sink { completion in
                expectation.fulfill()
            } receiveValue: { dataType in
                data = dataType
            }
            .store(in: &subscriptionBag)

        
        wait(for: [expectation], timeout: 2.0)
        
        XCTAssertTrue(data?.fakeText == fakeText)
    }
    
    private func fakePublisher(result: Result<(data: Data, response: URLResponse), URLError>) -> AnyPublisher<(data: Data, response: URLResponse), URLError> {
        Future<(data: Data, response: URLResponse), URLError>() { promise in promise(result) }.eraseToAnyPublisher()
    }
}

// Helpers

struct FakeRequest: HTTPRequest {
    
    typealias ResponseType = DataType
    
    let endpointString: String
    let httpMethod: HTTPMethod = .get
    
    init(endpointString: String = "https://fake.endpoint") {
        self.endpointString =  endpointString
    }
}

struct DataType: Codable {
    let fakeText: String
}

extension HTTPURLResponse {
    static func withStatusCode(_ code: Int) -> Self {
        HTTPURLResponse(url: URL(string: "https:/some.url")!, statusCode: code, httpVersion: nil, headerFields: nil) as! Self
    }
}
