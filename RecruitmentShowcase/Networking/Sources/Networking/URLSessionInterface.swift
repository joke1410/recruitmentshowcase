//
//  URLSessionInterface.swift
//

import Foundation
import Combine

public protocol URLSessionProxyInterface {
    func dataTaskPublisher(for request: URLRequest) -> AnyPublisher<(data: Data, response: URLResponse), URLError>
}

final public class URLSessionProxy: URLSessionProxyInterface  {
    
    private var session: URLSessionInterface
    
    public init(session: URLSessionInterface) {
        self.session = session
    }
    
    public func dataTaskPublisher(for request: URLRequest) -> AnyPublisher<(data: Data, response: URLResponse), URLError> {
        session.dataTaskPublisher(for: request).eraseToAnyPublisher()
    }
}

public protocol URLSessionInterface  {
    func dataTaskPublisher(for request: URLRequest) -> URLSession.DataTaskPublisher
}

extension URLSession: URLSessionInterface { }
