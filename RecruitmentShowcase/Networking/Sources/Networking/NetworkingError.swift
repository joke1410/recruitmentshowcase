//
//  NetworkingError.swift
//

import Foundation

public enum NetworkingError: Error {
    
    public enum Internal: Error {
        case invalidEndpoint
    }
    
    public struct Network: Error {
        public enum Code: Int, Error {
            case badResponse = 400
            case unauthorized = 401
            case forbidden = 403
            case notFound = 404
            case notAllowed = 405
            case notAcceptable = 406
            case requestTimeout = 408
            case conflict = 409
            case gone = 410
            case serverError = 500
            case badGateway = 502
            case serviceUnavailable = 503
            case gatwwayTimeout = 504
            case networkAuthenticationRequired = 511
        }
        public var code: Code
        public var data: Data?
        
        public init?(rawCodeValue: Code.RawValue, data: Data? = nil) {
            guard let code = Code(rawValue: rawCodeValue) else { return nil }
            self.code = code
            self.data = data
        }
    }
    case `internal`(underlying: Internal)
    case network(underlying: Network)
    case encoding(underlying: EncodingError)
    case decoding(underlying: DecodingError)
    case other(underlying: Error)
}

extension NetworkingError.Network {
    init?(response: URLResponse?, data: Data?) {
        guard let httpResponse = response as? HTTPURLResponse else { return nil }
        if (200...209) ~= httpResponse.statusCode { return nil }
        self.init(rawCodeValue: httpResponse.statusCode, data: data)
    }
}

extension NetworkingError {
    public init(with error: Error) {
        switch error {
        case let error as Internal:
            self = .internal(underlying: error)
        case let error as Network:
            self = .network(underlying: error)
        case let error as EncodingError:
            self = .encoding(underlying: error)
        case let error as DecodingError:
            self = .decoding(underlying: error)
        case let error as NetworkingError:
            self = error
        default:
            self = .other(underlying: error)
        }
    }
}
