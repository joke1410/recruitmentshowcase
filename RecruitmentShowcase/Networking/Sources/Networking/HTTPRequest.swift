//
//  HTTPRequest.swift
//

import Foundation

public protocol HTTPRequest {
    associatedtype RequestBodyType: Encodable
    associatedtype ResponseType: Decodable

    var endpointString: String { get }
    var httpMethod: HTTPMethod { get }
    var additionalPathComponents: [String]? { get }
    var customHeaders: [String: String]? { get }
    var body: HTTPBody<RequestBodyType>? { get }
}

public extension HTTPRequest {
    var additionalPathComponents: [String]? { nil }
    var customHeaders: [String: String]? { nil }
}

public extension HTTPRequest where RequestBodyType == NoType {
    var body: HTTPBody<NoType>? { nil }
}

public struct NoType: Codable { }

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}

public struct HTTPBody<T: Encodable> {
    let contentType: ContentType
    let content: T
    
    public init(contentType: ContentType, content: T) {
        self.contentType = contentType
        self.content = content
    }
}

public enum ContentType {
    case json
    
    var underlyingValue: String {
        switch self {
        case .json: return "application/json"
        }
    }
    
    static let headerName = "Content-Type"
}
