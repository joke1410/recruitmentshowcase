//
//  NetworkClient.swift
//

import Foundation
import Combine

public protocol NetworkClientInterface {
    func makeRequest<T: HTTPRequest>(_ request: T) -> AnyPublisher<T.ResponseType, NetworkingError>
}

final public class NetworkClient: NetworkClientInterface {
    
    private let sessionProxy: URLSessionProxyInterface
    
    public convenience init(session: URLSessionInterface) {
        self.init(sessionProxy: URLSessionProxy(session: session))
    }
    
    init(sessionProxy: URLSessionProxyInterface = URLSessionProxy(session: URLSession.shared)) {
        self.sessionProxy = sessionProxy
    }
    
    public func makeRequest<T: HTTPRequest>(_ request: T) -> AnyPublisher<T.ResponseType, NetworkingError> {
        Just<T>(request)
            .setFailureType(to: NetworkingError.self)
            .tryMap { try constructURLRequest(basedOn: $0) }
            .flatMap { [unowned self] request in
                self.sessionProxy
                    .dataTaskPublisher(for: request)
                    .mapError { NetworkingError.other(underlying: $0) }
            }
            .tryMap { (data, response) -> Data in
                if let error = NetworkingError.Network.init(response: response, data: data) {
                    throw NetworkingError.network(underlying: error)
                } else {
                    return data
                }
            }
            .tryMap { data in
                if T.ResponseType.self == NoType.self {
                    return NoType() as! T.ResponseType
                }  else  {
                    do {
                        let decoder = JSONDecoder()
                        decoder.dateDecodingStrategy = .iso8601
                        return try decoder.decode(T.ResponseType.self, from: data)
                    } catch {
                        throw NetworkingError(with: error)
                    }
                }
            }
            .mapError { NetworkingError(with: $0) }
            .eraseToAnyPublisher()
    }
}

private extension NetworkClient {
    
    func constructURLRequest<T: HTTPRequest>(basedOn request: T) throws -> URLRequest {
        guard var url = URL(string: request.endpointString) else {
            throw NetworkingError.internal(underlying: .invalidEndpoint)
        }
        
        request.additionalPathComponents?.forEach { pathComponent in
            url.appendPathComponent(pathComponent)
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.httpMethod.rawValue
        
        if let headers = request.customHeaders {
            for (key, value) in headers {
                urlRequest.setValue(value, forHTTPHeaderField: key)
            }
        }
        
        if let body = request.body {
            urlRequest.setValue(body.contentType.underlyingValue, forHTTPHeaderField: ContentType.headerName)
            do {
                let encoder = JSONEncoder()
                encoder.dateEncodingStrategy = .iso8601
                urlRequest.httpBody = try encoder.encode(body.content)
            } catch {
                throw NetworkingError(with: error)
            }
        }
        return urlRequest
    }
}
